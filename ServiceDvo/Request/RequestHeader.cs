﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Request
{
    public class RequestHeader
    {
        public String Name { get; set; }

        public String Value { get; set; }
    }
}
