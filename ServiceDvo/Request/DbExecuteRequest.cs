﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Request
{
    public class DbExecuteRequest
    {
        public String ConnStr { get; set; }

        public String Sql { get; set; }

    }
}
