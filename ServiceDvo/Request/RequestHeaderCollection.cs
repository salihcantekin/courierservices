﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Request
{
    public class RequestHeaderCollection : List<RequestHeader>
    {
        public void Add(String name, String value)
        {
            Add(new RequestHeader() { Name = name, Value = value });
        }
    }
}
