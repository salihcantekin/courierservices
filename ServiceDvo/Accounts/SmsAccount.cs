﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Accounts
{
    public class SmsAccount
    {
        public Guid Guid { get; set; }
        public String Name { get; set; }
        public bool IsActive { get; set; }

        public object CustomSettings { get; set; }
    }
}
