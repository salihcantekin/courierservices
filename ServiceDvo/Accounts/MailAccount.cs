﻿using ServiceDvo.Enums;
using ServiceDvo.Interfaces;
using ServiceDvo.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Accounts
{
    public class MailAccount
    {
        public MailAccount()
        {
        }

        public Guid Guid { get; set; }

        public DateTime CreateDate { get; set; }

        public String Name { get; set; }

        public String SenderMail { get; set; }

        public String SenderDisplayName { get; set; }

        public String HostName { get; set; }

        public int Port { get; set; }

        public bool UseSsl { get; set; }

        public bool UseAuthentication { get; set; }

        public UserAuth UserAuth { get; set; }

        public MailSendType SendType { get; set; }

        public String Charset { get; set; }

        public String NoReplyMail { get; set; }
    }
}
