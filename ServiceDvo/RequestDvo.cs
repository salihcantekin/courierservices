﻿using ServiceDvo.Request;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo
{
    public class RequestDvo: IDisposable
    {
        public RequestDvo()
        {
            ContentType = "application/x-www-form-urlencoded";
            Method = "POST";
            HeaderList = new RequestHeaderCollection();
            TimeOut = 300;
        }


        public String Url { get; set; }

        public String Content { get; set; }
        public String ContentType { get; set; }

        public String Method { get; set; }

        /// <summary>
        /// Timeout value as second
        /// </summary>
        public int? TimeOut { get; set; }

        public String AcceptType { get; set; }

        public RequestHeaderCollection HeaderList { get; set; }

        public void Dispose()
        {
            HeaderList = null;
            AcceptType = null;
            TimeOut = null;
            Url = null;
            Method = null;
        }
    }
}
