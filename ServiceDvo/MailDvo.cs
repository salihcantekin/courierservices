﻿using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo
{
    public class MailDvo
    {
        public MailDvo()
        {
            Encoding = Encoding.UTF8;
            Priority = MailPriority.Normal;
        }

        public Guid Guid { get; set; }

        public String Subject { get; set; }

        public String MailBody { get; set; }

        public List<String> To { get; set; }

        public List<String> CC { get; set; }

        public List<String> Bcc { get; set; }

        public String SenderDisplayName { get; set; }

        public MailAccount Account { get; set; }

        public MailProfile Profile { get; set; }

        public Guid AccountGuid { get; set; }

        public Guid ProfileGuid { get; set; }

        public bool IsHtmlBody { get; set; }

        public MailPriority Priority { get; set; }

        public Encoding Encoding { get; set; }
    }
}
