﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo
{
    public enum MailPriority
    {
        High = 0,
        Normal = 1,
        Low = 2
    }

    public enum AttachmentType
    {
        DownloadFromUrl = 1,
        FileCopy = 2
    }
}
