﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo
{
    public class MobilDevSmsDvo: Base.BaseSms
    {
        public MobilDevSmsDvo()
        {
            Language = "turkish";
            BlackList = 0;
            SendType = 1;
        }

        public String Key { get; set; }

        public String Secret { get; set; }

        public String Channel { get; set; }

        public String MsiSdn { get; set; }

        public String Originator { get; set; }

        public int BlackList { get; set; }

        public int SendType { get; set; }

        public String Url { get; set; }

        public String Language { get; set; }
    }
}
