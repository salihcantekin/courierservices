﻿using ServiceDvo.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Request
{
    public class RequestResult : BaseResult
    {
        public HttpStatusCode StatusCode { get; set; }

        public WebHeaderCollection Headers { get; set; }

        public String ResponseString { get; set; }

        public override string ToString()
        {
            return $@"IsSuccess: {IsSuccess.ToString()} {Environment.NewLine}
                      ErrorMessage:{ErrorMessage} {Environment.NewLine}
                      StatusCode: {StatusCode.ToString()} {Environment.NewLine}
                      ResponseString: {ResponseString}";
        }
    }
}
