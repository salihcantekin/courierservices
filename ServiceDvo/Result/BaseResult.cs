﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Result
{
    public class BaseResult
    {
        public bool IsSuccess { get; set; }
        public String ErrorMessage { get; set; }

        public void SetError(Exception ex)
        {
            SetException(ex.Message);
        }
            
        public void SetException(String message)
        {
            IsSuccess = false;
            ErrorMessage = message;
        }
    }
}
