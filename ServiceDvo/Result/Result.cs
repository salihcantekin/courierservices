﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Result
{
    public class Result<T> : BaseResult
    {
        public T Value { get; set; }
    }
}
