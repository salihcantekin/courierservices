﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Result
{
    public class SmsResult: BaseResult
    {
        public String Message { get; set; }

        public String UniqueId { get; set; }
    }
}
