﻿using ServiceDvo.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Profiles
{
    public class MailProfile 
    {
        public MailProfile()
        {
            //MailAccount = new MailAccount();
        }

        public Guid Guid { get; set; }
        public DateTime CreateDate { get; set; }

        public String Name { get; set; }

        public String SenderDisplayName { get; set; }

        public bool IsActive { get; set; }

        public MailAccount MailAccount { get; set; }


    }
}
