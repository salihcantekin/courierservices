﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Base
{
    public class BaseSms
    {
        public String PhoneNumber { get; set; }

        public String Message { get; set; }

    }
}
