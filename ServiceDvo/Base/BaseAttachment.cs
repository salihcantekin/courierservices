﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDvo.Base
{
    public abstract class BaseAttachment
    {
        public String FileName { get; set; }

        protected String Extension { get; set; }

        public String LocalPath { get; set; }

        public AttachmentType AttachmentType { get; set; }

        public abstract String GetLocalFilePath();
    }
}
