﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CourierServices.DVO
{
    [DataContract]
    public class MailResponse
    {
        [DataMember]
        public Guid UniqueId { get; set; }

        [DataMember]
        public String Message { get; set; }
    }
}