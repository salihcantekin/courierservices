﻿using CourierServices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.DVO
{
    public class SmsAccountDVO: BaseDVO
    {
        public SmsAccountDVO(object Value):base(Value)
        {
            BaseType = typeof(BaseSmsAccount);
        }

        public SmsAccountDVO(): this(null)
        {

        }

        public Guid Guid { get; set; }
        public String Name { get; set; }
        public bool IsActive { get; set; }

        public override void Load(object Value)
        {
            if (Value == null)
                return;

            base.Load(Value);

            BaseSmsAccount account = (BaseSmsAccount)Value;

            Guid = account.Guid;
            IsActive = account.IsActive;
            Name = account.Name;
        }
    }
}