﻿using CourierServices.Base;
using CourierServices.Enums;
using CourierServices.Profiles;
using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CourierServices.DVO
{
    public class MailProfileDVO: BaseDVO
    {
        public MailProfileDVO(object Value) : base(Value)
        {
            BaseType = typeof(MailProfile);
        }

        public MailProfileDVO()
        {

        }

        public Guid Guid { get; set; }

        public String Name { get; set; }

        public MailSendType MailType { get; set; }

        public MailAccount Account { get; set; }

        public override void Load(object Value)
        {
            base.Load(Value);

            MailProfile profile = (MailProfile)Value;

            Guid = profile.Guid;
            Name = profile.Name;

            Account = profile.MailAccount;
        }
    }
}