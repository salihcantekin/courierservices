﻿using CourierServices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.DVO
{
    public class SmsProfileDVO : BaseDVO
    {
        public SmsProfileDVO(object Value) : base(Value)
        {
            BaseType = typeof(BaseSmsProfile);
        }

        public SmsProfileDVO() : this(null)
        {

        }

        public Guid Guid { get; set; }

        public String Name { get; set; }

        public bool IsActive { get; set; }

        public SmsAccountDVO Account { get; set; }

        public override void Load(object Value)
        {
            base.Load(Value);

            BaseSmsProfile profile = (BaseSmsProfile)Value;

            Guid = profile.Guid;
            IsActive = profile.IsActive;
            Name = profile.Name;

            Account = new SmsAccountDVO();
            Account.Load(profile.Account);
        }
    }
}