﻿using CourierServices.Base;
using CourierServices.CustomSettings;
using CourierServices.DVO;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Linq;

namespace CourierServices.Sms
{
    public class MobilDevSms : BaseSms
    {
        private MobilDevSettings Settings;


        public override SmsResponse Send()
        {
            Settings = (MobilDevSettings)Account.CustomSettings;
            
            SmsResponse Result = new SmsResponse();

            RestClient Client = new RestClient(Settings.RootUrl);

            RestRequest Req = new RestRequest(Method.POST) { RequestFormat = DataFormat.Json };

            Req.JsonSerializer.ContentType = Settings.ContentType;

            Req.AddBody($"{PhoneNumber} {Content}");

            Settings.
                ParameterList.
                ToList().
                ForEach(i => Req.AddQueryParameter(i.Key, i.Value.ToString()));

            IRestResponse res = Client.Post(Req);

            if (res.StatusCode != System.Net.HttpStatusCode.OK)
                Result.Message = res.ErrorMessage;
            else
                Result.Message = res.Content;


            JToken token = JObject.Parse(res.Content);
            if (token.SelectToken("error") != null)
                throw new Exception(token.SelectToken("Message").ToString());

            //if (token.SelectToken("queueId") != null)
            //    Result.Value.SmsResultText = token.SelectToken("queueId").ToString();

            token = null;

            return Result;
        }
    }
}