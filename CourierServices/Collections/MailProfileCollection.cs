﻿using CourierServices.Base;
using CourierServices.CustomExceptions;
using CourierServices.Profiles;
using DBManager;
using DBManager.Interfaces;
using ServiceDvo.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Collections
{
    public class MailProfileCollection : BaseList<Guid, MailProfile>
    {
        protected override Guid GetKeyForItem(MailProfile item)
        {
            return item.Guid;
        }

        public MailProfileCollection() : base()
        {
            LoadAll();
        }

        public override void LoadAll()
        {
            String Sql = @"SELECT  a.*
							  FROM mail_profiles a
							 ORDER BY a.profile_name";

            using (DbTran db = new DbTran())
            {
                IDbDataReader dr = db.Provider.ExecuteReader(Sql);

                while (dr.Read())
                {
                    MailProfile Profile = new MailProfile();
                    LoadFromReader(dr, Profile);
                    Add(Profile);
                }
            }
        }

        public void LoadFromReader(IDbDataReader dr, MailProfile Profile)
        {
            Profile.Guid = dr.GetValue<Guid>("GUID");
            Profile.CreateDate = dr.GetValue<DateTime>("CREATE_DATE");
            Profile.Name = dr.GetString("PROFILE_NAME");
            Profile.IsActive = dr.GetValue<bool>("PROFILE_NAME");
            Profile.SenderDisplayName = dr.GetString("SENDER_DISPLAY_NAME");
            Guid accountGuid = dr.GetValue<Guid>("ACCOUNT_GUID");

            Profile.MailAccount = Constants.MailAccountCollection.FindValue(accountGuid);
            if (Profile.MailAccount == null)
                throw new ChildNotFoundException($"The mail account not found on the AccountList. Guid: {accountGuid}");

            if (Profile.MailAccount != null && String.IsNullOrEmpty(Profile.SenderDisplayName))
                Profile.SenderDisplayName = Profile.MailAccount.SenderMail;
        }

    }
}