﻿using CourierServices.Attachment;
using CourierServices.Base;
using CourierServices.Enums;
using ServiceDvo.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Collections
{
    public class AttachmentCollection : BaseList<int, BaseAttachment>
    {
        private List<BaseAttachment> attList;
        int Counter;

        protected override int GetKeyForItem(BaseAttachment item)
        {
            return 0;
        }

        public AttachmentCollection():base()
        {
            attList = new List<BaseAttachment>();
            Counter = 0;

            LoadAll();
        }

        public void Add(Attachment.Attachment Att)
        {
            BaseAttachment baseAtt = null;

            if (Att.AttType == AttachmentType.DownloadFromUrl)
                baseAtt = new UrlAttachment(Att.FullFilePath, Att.LocalDir);
            else if (Att.AttType == AttachmentType.FileCopy)
                baseAtt = new FileCopyAttachment(Att.FullFilePath, Att.LocalDir);

            if (!String.IsNullOrEmpty(Att.FileName))
                baseAtt.FileName = Att.FileName;

            Add(++Counter, baseAtt);
        }
    }
}