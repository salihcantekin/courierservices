﻿using CourierServices.Base;
using CourierServices.DVO;
using CourierServices.Profiles;
using DBManager;
using DBManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Collections
{
    public class SmsProfileCollection : BaseList<Guid, SmsProfile>
    {
        protected override Guid GetKeyForItem(SmsProfile item)
        {
            return item.Guid;
        }

        public SmsProfileCollection() : base()
        {
            LoadAll();
        }

        public override void LoadAll()
        {
            base.LoadAll();

            String Sql = @"SELECT CASE
								   WHEN NVL (b.sender, 'YOK') = 'YOK' THEN a.profile_name
								   ELSE b.sender
							   END
								   AS sender,
							   a.*
						  FROM     sms_profiles a
							   LEFT JOIN
								   altar_sms.sms_user_sender b
							   ON a.profile_name = b.user_name";

            using (DbTran db = new DbTran())
            {
                IDbDataReader dr = db.Provider.ExecuteReader(Sql);
                while (dr.Read())
                {
                    SmsProfile Profile = new SmsProfile();
                    LoadFromReader(dr, Profile);
                    Add(Profile);
                }
            }
        }

        private void LoadFromReader(IDbDataReader dr, BaseSmsProfile Profile)
        {
            Profile.Guid = dr.GetValue<Guid>("GUID");
            //Profile.CreateDate = DbTools.Read<DateTime>(dr, "CREATE_DATE");
            Profile.Name = dr.GetString("PROFILE_NAME");
            //Profile.Status = DbTools.Read<bool>(dr, "STATUS");
            //Profile.Sender = DbTools.Read<String>(dr, "SENDER");
            Guid accountId = dr.GetValue<Guid>("ACCOUNT_GUID");

            Profile.Account = Constants.MobilDevSmsAccountCollection.FindValue(accountId);
        }
    }
}