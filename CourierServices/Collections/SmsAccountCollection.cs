﻿using CourierServices.Base;
using CourierServices.CustomSettings;
using CourierServices.DVO;
using DBManager;
using DBManager.Interfaces;
using ServiceDvo.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CourierServices.Collections
{
    public class SmsAccountCollection : BaseList<Guid, SmsAccount>
    {
        List<SmsAccount> accountList;

        protected override Guid GetKeyForItem(SmsAccount item)
        {
            return item.Guid;
        }

        public SmsAccountCollection() : base()
        {
            accountList = new List<SmsAccount>
            {
                new SmsAccount() { Guid = Guid.NewGuid() }
            };

            LoadAll();
        }

        // TODO 1
        // new MobilDevSmsAccount() yaratma işi servis tarafında hata veriyor
        public override void LoadAll()
        {
            base.LoadAll();

            String Sql = $"SELECT * FROM {GetTableName()}";

            using (DbTran db = new DbTran())
            {
                IDbDataReader dr = db.Provider.ExecuteReader(Sql);

                while (dr.Read())
                {
                    SmsAccount account = new SmsAccount();
                    LoadFromReader(dr, account);
                    Add(account);
                }
            }

            //accountList.AsParallel().ForAll(i => i.Load());
            AddRange(accountList);
        }


        private void LoadFromReader(IDbDataReader dr, SmsAccount account)
        {
            account.Guid = dr.GetValue<Guid>("GUID");
            account.Name = "MobilDev1";
            account.IsActive = true;

            MobilDevSettings settings = new MobilDevSettings
            {
                RootUrl = dr.GetString("ROOT_URL"),
                ParameterList = new Dictionary<string, object>()
                {
                    ["key"] = dr.GetString("KEY"),
                    ["secret"] = dr.GetString("SCRET"),
                    ["originator"] = dr.GetString("DEFAULTORIGINATOR"),
                    ["msisdn"] = dr.GetString("MSI_SDN"),
                    ["blacklist"] = dr.GetInt("BLACKLIST").ToString(),
                    ["channel"] = dr.GetString("CHANNEL"),
                    ["sendType"] = "1",
                    ["language"] = "turkish"
                }
            };

            account.CustomSettings = settings;
        }

        public string GetTableName()
        {
            return "SMS_ACCOUNT";
        }
    }
}