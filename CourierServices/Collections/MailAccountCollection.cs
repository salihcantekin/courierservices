﻿using CourierServices.Base;
using DBManager;
using DBManager.Interfaces;
using ServiceDvo.Accounts;
using System;

namespace CourierServices.Collections
{
    public class MailAccountCollection : BaseList<Guid, MailAccount>
    {
        protected override Guid GetKeyForItem(MailAccount item)
        {
            return item.Guid;
        }

        public MailAccountCollection() : base()
        {
            LoadAll();
        }

        public override void LoadAll()
        {
            base.LoadAll();

            String Sql = "SELECT * FROM MAIL_ACCOUNTS";
            using (DbTran db = new DbTran())
            {
                IDbDataReader dr = db.Provider.ExecuteReader(Sql);
                while (dr.Read())
                {
                    MailAccount Account = new MailAccount();
                    LoadFromReader(dr, Account);
                    Add(Account);
                }
            }
        }

        private void LoadFromReader(IDbDataReader dr, MailAccount Account)
        {
            Account.Guid = dr.GetValue<Guid>("GUID");

            Account.CreateDate = dr.GetValue<DateTime>("CREATE_DATE");
            Account.Name = dr.GetString("NAME");
            Account.HostName = dr.GetString("HOST_ADDRESS");
            Account.Port = dr.GetInt("SMTP_PORT");

            Account.UserAuth = new ServiceDvo.User.UserAuth
            {
                UserName = dr.GetString("USERNAME"),
                Password = dr.GetString("USER_PASSWORD")
            };

            Account.SenderMail = dr.GetString("SENDER_MAIL");
            Account.UseSsl = dr.GetValue<bool>("USE_SSL");
            //Account.Channel = (SendType)DbTools.Read<int>(dr, "CHANNEL");
            Account.Charset = dr.GetString("CHARSET");
            Account.NoReplyMail = dr.GetString("NOREPLY"); 
        }
    }
}