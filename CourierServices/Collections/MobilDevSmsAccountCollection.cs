﻿using CourierServices.Base;
using DBManager;
using DBManager.Interfaces;
using ServiceDvo.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Collections
{
    public class MobilDevSmsAccountCollection : BaseList<Guid, MobilDevSmsAccount>
    {
        protected override Guid GetKeyForItem(MobilDevSmsAccount item)
        {
            return item.Guid;
        }

        //List<MobilDevSmsAccount> accountList;

        public MobilDevSmsAccountCollection()
        {
            //accountList = new List<MobilDevSmsAccount>();

            LoadAll();
        }

        public override void LoadAll()
        {
            base.LoadAll();

            String Sql = $"SELECT * FROM {GetTableName()} WHERE IS_MOBILDEV = 'Y' ";

            using (DbTran db = new DbTran())
            {
                IDbDataReader dr = db.Provider.ExecuteReader(Sql);

                while (dr.Read())
                {
                    MobilDevSmsAccount account = new MobilDevSmsAccount();
                    LoadFromReader(dr, account);
                    Add(account);
                }
            }

            //AddRange(accountList);
        }

        private string GetTableName()
        {
            return "SMS_ACCOUNT";
        }

        private void LoadFromReader(IDbDataReader dr, MobilDevSmsAccount account)
        {
            if (account == null)
                account = new MobilDevSmsAccount();

            account.Guid = dr.GetValue<Guid>("GUID");
            account.Url = dr.GetString("ROOT_URL");
            account.Key = dr.GetString("KEY");
            account.Secret = dr.GetString("SCRET");
            account.Channel = dr.GetString("CHANNEL");
            account.MsiSdn = dr.GetString("MSI_SDN");
            account.Originator = dr.GetString("DEFAULTORIGINATOR");
            account.BlackList = dr.GetInt("BLACKLIST");
            account.Language = "turkish";
            account.SendType = 1;
        }
    }
}