﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.CustomSettings
{
    public class EuroMessageMail
    {
        public String FromName { get; set; }

        public String FromAddress { get; set; }

        public String ReplyAddress { get; set; }

        public String Subject { get; set; }

        [JsonProperty(PropertyName = "HtmlBody")]
        public String HtmlBody { get; set; }

        public String ToName { get; set; }

        public String Charset { get; set; }

        public String ToEmailAddress { get; set; }

        public String PostType { get; set; }

        public String KeyId { get; set; }

        public String CustomParams { get; set; }

        public String CcAddress { get; set; }

        public String BccAddress { get; set; }
    }
}