﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.CustomSettings
{
    public class MobilDevSettings
    {
        public String ContentType { get { return "raw"; } }

        public String RootUrl { get; set; }

        internal Dictionary<String, object> ParameterList { get; set; }

        public MobilDevSettings()
        {
            
        }

        public String Key { get; set; }
        public String Secret { get; set; }
        public String Originator { get; set; }
        public String MsiSdn { get; set; }
        public int BlackList { get; set; }
        public String Channel { get; set; }
        public int SendType { get; set; }
        public String Language { get; set; }
    }
}