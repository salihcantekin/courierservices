﻿
using DBManager;
using DBManager.CSManager;
using ServiceDvo;
using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using ServiceDvo.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace CourierServices
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            //ConnectionString oracle = DBManagerSettings.Default.CSCollection.Add("ETSX_8", "Data Source=ETSX_8;User Id=ETS_VNL;Password=ETS_VNL;Persist Security Info=True;Pooling=true;", DbProviderType.OracleProvider);

            DBManagerSettings.DefaultConnStr = AppConfig.DefConnStr;
            DBManagerSettings.DefaultProvider = DbProviderType.OracleProvider;

            try
            {
                SenderService.Service.SendEuroMessageMail(new Guid("61aa3662-afdf-fe00-f6e0-30901000679c"));
            }
            catch (Exception ex)
            {
                Logger.Default.WriteToFile(ex.ToString());
                throw;
            }

            //SmsResult smsResult = SenderService.Service.SendMobilDevSms(new Guid("e2847d24-d500-4fcf-8947-20e87c62e9e0"), "05073674606", "Test İçerik");

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}