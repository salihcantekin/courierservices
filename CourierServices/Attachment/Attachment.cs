﻿using CourierServices.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Attachment
{
    public class Attachment
    {
        public String FullFilePath { get; set; }

        public String FileName { get; set; }

        public AttachmentType AttType { get; set; }

        public String LocalDir { get; set; }
    }
}