﻿using CourierServices.Base;
using ServiceDvo.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CourierServices.Attachment
{
    public class FileCopyAttachment : BaseAttachment
    {
        public FileCopyAttachment(String FilePath, String LocalFileDir)
        {
            this.FilePath = FilePath;
            AttachmentType = ServiceDvo.AttachmentType.FileCopy;
            LocalPath = LocalFileDir;
        }

        public FileCopyAttachment(String FilePath) : this(FilePath, Path.Combine(AppConfig.Current.DefaultAttDir, Path.GetFileName(FilePath))) { }


        public String FilePath { get; set; }

        public override string GetLocalFilePath()
        {
            try
            {
                if (!File.Exists(FilePath))
                    throw new FileNotFoundException("File not found", FilePath);

                String fileName = FileName ?? Path.GetFileName(FilePath);
                LocalPath = LocalPath ?? Path.Combine(AppConfig.Current.DefaultAttDir, Path.GetFileName(fileName));

                File.Copy(FilePath, Path.Combine(Path.GetDirectoryName(LocalPath), fileName), true);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

            return LocalPath;
        }

        
    }
}