﻿using CourierServices.Base;
using CourierServices.Utilities;
using ServiceDvo.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CourierServices.Attachment
{
    public class UrlAttachment : BaseAttachment
    {
        public Uri Url { get; set; }

        public UrlAttachment(String Url, String LocalFileName) : this(new Uri(Url), LocalFileName) { }

        public UrlAttachment(String Url) : this(Url, Path.Combine(AppConfig.Current.DefaultAttDir, Path.GetFileName(Url))) { }

        public UrlAttachment(Uri Url, String LocalFileName)
        {
            this.Url = Url;
            LocalPath = LocalFileName;
            AttachmentType = ServiceDvo.AttachmentType.DownloadFromUrl;
        }

        public override string GetLocalFilePath()
        {
            bool Success = FileDownloader.DownloadByUrl(Url, LocalPath);
            if (!Success)
                return String.Empty;

            return LocalPath;
        }
    }
}