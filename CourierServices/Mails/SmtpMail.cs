﻿using CourierServices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using CourierServices.Enums;
using System.Net.Mail;
using CourierServices.Factories;
using CourierServices.Attachment;
using ServiceDvo.Base;

namespace CourierServices.Mails
{
    public class SmtpMail : BaseMail
    {
        public override void ChangeStatus(MailStatus Status)
        {

        }

        public override void Send()
        {
            MailMessage Message = null;

            while (ToAddresses.Count > 0)
            {
                String toMailAddress = ToAddresses.First();
                ToAddresses.Remove(toMailAddress);

                Message = new MailMessage()
                {
                    Body = MailBody,
                    BodyEncoding = BodyEncoding,
                    From = new MailAddress(MailProfile.MailAccount.SenderMail, MailProfile.SenderDisplayName),
                    IsBodyHtml = IsHtmlContent,
                    Subject = Subject,
                    SubjectEncoding = SubjectEncoding
                };

                foreach (BaseAttachment baseAtt in Attachments.Values)
                {
                    Message.Attachments.Add(new System.Net.Mail.Attachment(baseAtt.GetLocalFilePath()) { Name = baseAtt.FileName });
                }

                Message.To.Add(new MailAddress(toMailAddress));
                CcAddresses?.ForEach(i => Message.CC.Add(i));
                BccAddresses?.ForEach(i => Message.Bcc.Add(i));
            }

            using (SmtpClient Client = SmtpClientList.Current.CreateByProfile(MailProfile))
                Client.Send(Message);
        }
    }
}