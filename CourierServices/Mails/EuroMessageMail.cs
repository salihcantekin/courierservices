﻿using CourierServices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CourierServices.Enums;
using System.Text;
using CourierServices.EuroMessageAuthService;
using CourierServices.EuroMessagePostService;
using CourierServices.Utilities;
using System.IO;

namespace CourierServices.Mails
{
    public class EuroMessageMail : BaseMail
    {
        public override void ChangeStatus(MailStatus Status)
        {

        }

        public override void Send()
        {
            StringBuilder Result = new StringBuilder();
            Auth authService = new Auth();
            Post postService = null;

            try
            {
                EmAuthResult authResult = authService.Login(MailProfile.MailAccount.UserAuth.UserName, MailProfile.MailAccount.UserAuth.Password);
                if (!authResult.Code.Equals("00"))
                {
                    Result.AppendFormat("#{0} EuroMessage Kullanıcı Doğrulaması Yapılamadı.\nHata: {1}", base.Guid.ToString(), authResult.Message);
                }

                String serviceTicket = authResult.ServiceTicket;

                while (ToAddresses.Count > 0)
                {
                    String To = ToAddresses.FirstOrDefault();
                    ToAddresses.Remove(To);

                    if (!Utils.IsValidatedMail(To))
                    {
                        Result.AppendLine(String.Format("#{0} numaralı iş için alıcı mail adresi alanı boş yada yanlış.!", Guid.ToString()));
                        continue;
                    }

                    EmPostResult postResult = null;
                    try
                    {
                        #region Attachment Process

                        List<EmAttachment> attList = new List<EmAttachment>
                        (
                            Attachments.
                                Values.
                                Select(i => i.GetLocalFilePath()).
                                Select(i => new EmAttachment()
                                {
                                    Name = Path.GetFileNameWithoutExtension(i),
                                    Type = Path.GetExtension(i).Replace(".", ""),
                                    Content = File.ReadAllBytes(i)
                                })
                        );

                        #endregion

                        postService = new Post();
                        postResult = postService.PostHtml(serviceTicket,
                            MailProfile.SenderDisplayName == "" ? MailProfile.MailAccount.SenderMail : MailProfile.SenderDisplayName,
                            MailProfile.MailAccount.SenderMail,
                            MailProfile.MailAccount.NoReplyMail,
                            Subject,
                            MailBody,
                            MailProfile.MailAccount.Charset,
                            To,
                            To,
                            attList.ToArray()
                            );

                        if (!postResult.Code.Equals("00"))
                            Result.AppendFormat("#{0} Mail Gönderimi Sırasında Hata.({1})\nHata: {2}", base.Guid.ToString(), To, postResult.Message);

                        // OnLog
                    }
                    catch (Exception Ex)
                    {
                        Result.Append(String.Format("#{0} {1} : Hata ({2})", base.Guid.ToString(), To, Ex.Message));
                    }
                    finally
                    {
                        postResult = null;
                    }
                }
            }
            catch (Exception Ex)
            {
                Result.Append(String.Format("#{0} {1} : Hata ({2})", base.Guid.ToString(), ToAddresses, Ex.Message));
            }
            finally
            {
                authService = null;
                postService = null;
            }
        }
    }
}