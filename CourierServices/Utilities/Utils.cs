﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CourierServices.Utilities
{
    public class Utils
    {
        public static bool IsValidatedMail(String MailAddress)
        {
            if (String.IsNullOrEmpty(MailAddress) || String.IsNullOrWhiteSpace(MailAddress) || MailAddress.Length <= 4 || MailAddress.IndexOf("@") <= -1)
                return false;

            String Pattern = @"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])";

            return System.Text.RegularExpressions.Regex.IsMatch(MailAddress, Pattern);
        }

        public static String JsonToString<T>(T value)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

            String json = JsonConvert.SerializeObject(value, Formatting.Indented, settings);
            
            return json;
        }
    }
}