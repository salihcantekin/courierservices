﻿using System;
using System.Net;

namespace CourierServices.Utilities
{
    public static class FileDownloader
    {
        public static bool DownloadByUrl(String Url, String LocalFileName = null)
        {
            using (WebClient Client = new WebClient())
            {
                Client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);

                if (String.IsNullOrEmpty(LocalFileName))
                {
                    String localFileName = "{0:0000}_{1:00}_{2:00}{3}";
                    DateTime Now = DateTime.Now;
                    LocalFileName = String.Format(localFileName, Now.Year, Now.Month, Now.Day, ".txt");
                }

                Client.DownloadFile(Url.Replace(@"\", "/"), LocalFileName);
            }

            return true;
        }

        public static bool DownloadByUrl(Uri Url, String LocalFileName = null)
        {
            return DownloadByUrl(Url.ToString(), LocalFileName);
        }
    }
}