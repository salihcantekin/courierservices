﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CourierServices
{
    public class AppConfig
    {

        private static AppConfig current;
        public static AppConfig Current => current ?? (current = new AppConfig());

        public String DefaultAttDir => "C:\\Att";

        public static String DefConnStr => ConfigurationManager.ConnectionStrings["DefaultConnStr"].ConnectionString;
        //public static String DefConnStr => "User Id=ETS_VNL;Password=ETS_VNL;Data Source=ETSX_8";
    }
}