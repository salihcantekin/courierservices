﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CourierServices.Enums
{
    [Flags]
    public enum MailStatus
    {
        Pending = 1,
        Sending = 2,
        Sent = 4,
        WillTryAgain = 8,
        Failed = 16
    }

    [Flags]
    public enum MailSendType
    {
        Smtp = 1,
        EuroMessage = 2
    }

    public enum AttachmentType
    {
        DownloadFromUrl = 1,
        FileCopy = 2
    }

    public enum SmsSendType
    {
        MobilDevSms = 1
    }
}