﻿using CourierServices.DVO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Interfaces
{
    public interface IBaseSms
    {
        String PhoneNumber { get; set; }

        String Message { get; set; }

        object CustomSettings { get; set; }

        SmsResponse Send();
    }
}