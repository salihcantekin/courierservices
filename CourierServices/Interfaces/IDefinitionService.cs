﻿using CourierServices.Base;
using CourierServices.Collections;
using CourierServices.DVO;
using CourierServices.Profiles;

using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using ServiceDvo.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CourierServices.Interfaces
{
    [ServiceContract]
    interface IDefinitionService
    {
        

        #region SmsProfile Operations

        [OperationContract]
        BaseResult SaveSmsProfile(SmsProfile Account);

        //[OperationContract]
        //Result<List<SmsProfileDVO>> GetSmsProfiles();

        //[OperationContract]
        //BaseResult DeleteSmsProfile(Guid ProfileId);

        #endregion

        #region SmsAccount Operations

        [OperationContract]
        Result<List<SmsAccountDVO>> GetSmsAccounts();

        //[OperationContract]
        //BaseResult DeleteSmsAccount(Guid ProfileId);

        //[OperationContract]
        //BaseResponse SaveSmsAccount(SmsAccountDVO Account);

        #endregion
    }
}
