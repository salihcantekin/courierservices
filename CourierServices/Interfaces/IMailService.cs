﻿using CourierServices.Base;
using CourierServices.DVO;

using ServiceDvo;
using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using ServiceDvo.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CourierServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IMailService
    {

        [OperationContract]
        Result<List<MailProfile>> GetProfileDVOList();

        [OperationContract]
        Result<MailResult> Send(MailDvo mailDvo);

        #region MailProfile Operations

        [OperationContract]
        Result<List<MailProfile>> GetMailProfiles();

        [OperationContract]
        BaseResult SaveMailProfile(MailProfile Profile);

        [OperationContract]
        BaseResult DeleteMailProfile(Guid ProfileId);

        #endregion

        #region MailAccount Operations

        [OperationContract]
        BaseResult SaveMailAccount(MailAccount Account);

        [OperationContract]
        Result<List<MailAccount>> GetMailAccounts();

        [OperationContract]
        BaseResult DeleteMailAccount(Guid ProfileId);

        #endregion
    }
}
