﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Interfaces
{
    public interface ICustomSettings
    {
        object GetSettings();
    }
}