﻿using CourierServices.Base;
using CourierServices.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Interfaces
{
    public interface IBaseMail
    {
        void Send();

        void ChangeStatus(MailStatus Status);
        
    }
}