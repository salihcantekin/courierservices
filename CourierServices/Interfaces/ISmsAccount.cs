﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierServices.Interfaces
{
    interface ISmsAccount
    {
        void Load();
        object GetCustomSettings();
    }
}
