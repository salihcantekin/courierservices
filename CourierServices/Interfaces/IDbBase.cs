﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierServices.Interfaces
{
    interface IDBBase
    {
        void Save();
        int Delete();
        int Insert();
        int Update();
        String GetTableName();
    }
}
