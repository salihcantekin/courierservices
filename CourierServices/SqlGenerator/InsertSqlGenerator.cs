﻿using CourierServices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.SqlGenerator
{
    public class InsertSqlGenerator : BaseSqlGenerator
    {
        public InsertSqlGenerator(string TableName) : base(TableName)
        {
        }

        public InsertSqlGenerator()
        {

        }

        public override String Generate()
        {
            base.Generate();

            return $"INSERT INTO {TableName}({GenerateParams()}) VALUES({GenerateConditions()})";
        }

        protected override string GenerateParams()
        {
            return String.Join(",", paramList.Keys.ToArray());
        }

        protected override string GenerateConditions()
        {
            return String.Join(",", paramList.Values.Select(i => PrepareParam(i)));
        }
    }
}