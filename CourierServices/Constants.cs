﻿using CourierServices.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices
{
    public class Constants
    {
        private static MailProfileCollection mailProfileCollection;
        public static MailProfileCollection MailProfileCollection => mailProfileCollection ?? (mailProfileCollection = new MailProfileCollection());

        private static MailAccountCollection mailAccountCollection;
        public static MailAccountCollection MailAccountCollection => mailAccountCollection ?? (mailAccountCollection = new MailAccountCollection());

        

        //private static SmsProfileCollection smsProfileCollection;
        //public static SmsProfileCollection SmsProfileCollection => smsProfileCollection ?? (smsProfileCollection = new SmsProfileCollection());


        private static MobilDevSmsAccountCollection mobilDevSmsAccountCollection;
        public static MobilDevSmsAccountCollection MobilDevSmsAccountCollection => mobilDevSmsAccountCollection ?? (mobilDevSmsAccountCollection = new MobilDevSmsAccountCollection());

        //private static SmsAccountCollection smsAccountCollection;
        //public static SmsAccountCollection SmsAccountCollection => smsAccountCollection ?? (smsAccountCollection = new SmsAccountCollection());
    }
}