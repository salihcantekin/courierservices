﻿using CourierServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;
using System.Xml.Serialization;

namespace CourierServices.Base
{

    public abstract class BaseSmsAccount: IDBBase
    {
        public Guid Guid { get; set; }
        public String Name { get; set; }
        public bool IsActive { get; set; }

        public object Settings { get; set; }

        public abstract object GetCustomSettings();

        public abstract void Load();

        public abstract int Delete();

        public abstract int Insert();

        public abstract void Save();

        public abstract int Update();

        public abstract String GetTableName();
    }
}