﻿using CourierServices.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CourierServices.Base
{

    public abstract class BaseList<TKey, TValue> : IDisposable, IDictionary<TKey, TValue>
    {
        private Dictionary<TKey, TValue> List;

        protected BaseList()
        {
            if (typeof(TKey) == typeof(String))
                List = (new Dictionary<String, TValue>(StringComparer.OrdinalIgnoreCase)) as Dictionary<TKey, TValue>;
            else
                List = new Dictionary<TKey, TValue>();
        }

        public virtual TValue FindValue(TKey Key)
        {
            if (Key == null)
                return default(TValue);

            List.TryGetValue(Key, out TValue Value);
            return Value;
        }

        public void UpdateOrInsert(TKey Key, TValue Value)
        {
            if (List.ContainsKey(Key))
                List[Key] = Value;
            else
                List.Add(Key, Value);
        }

        public virtual void Replace(TKey Key, TValue Value)
        {
            if (List.ContainsKey(Key))
                List[Key] = Value;
        }

        public virtual void Replace(TKey Key, TValue Value, bool throwExceptionIfNotFound)
        {
            if (List.ContainsKey(Key))
                List[Key] = Value;
            else
            {
                if (throwExceptionIfNotFound)
                    throw new NotFoundInListException("The key not founded in list");
            }
        }

        public void ClearAll()
        {
            if (List.Count > 0)
                List.Clear();
        }

        public TValue this[TKey Key] { get => List[Key]; set => List[Key] = value; }

        public void Dispose()
        {
            //ClearAll();
            List = null;
        }

        public virtual void Add(TKey key, TValue value)
        {
            //if (List.ContainsKey(key))
            //    throw new Exception("The key already exists");
            List.Add(key, value);
        }

        public virtual void Add(TValue Value)
        {
            Add(GetKeyForItem(Value), Value);
        }

        public virtual void AddRange(IEnumerable<TValue> Values)
        {
            foreach (var item in Values)
                Add(item);
        }

        public virtual bool ContainsKey(TKey key) => List.ContainsKey(key);

        public virtual ICollection<TKey> Keys => List.Keys;

        public virtual bool Remove(TKey key) => List.Remove(key);


        public virtual bool TryGetValue(TKey key, out TValue value) => List.TryGetValue(key, out value);
        
        public virtual ICollection<TValue> Values => List.Values;


        public virtual void Add(KeyValuePair<TKey, TValue> item)
        {
            List.Add(item.Key, item.Value);
        }

        public virtual void Clear()
        {
            List.Clear();
        }

        public virtual bool Contains(KeyValuePair<TKey, TValue> item) => List.Contains(item);

        public virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            List.ToArray().CopyTo(array, arrayIndex);
        }

        public virtual int Count => List.Count;

        public virtual bool IsReadOnly => false;

        public virtual bool Remove(KeyValuePair<TKey, TValue> item) => List.Remove(item.Key);

        public virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => List.GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }

        protected abstract TKey GetKeyForItem(TValue item);

        public virtual void LoadAll()
        {

        }

        public List<TValue> ToList() => Values.ToList();
    }
}