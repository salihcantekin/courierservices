﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CourierServices.Base
{
    public class BaseSqlGenerator : IDisposable
    {
        protected Dictionary<String, object> paramList;
        protected Dictionary<String, object> conditionList;
        protected String TableName { get; set; }

        public BaseSqlGenerator(String TableName) : this()
        {
            this.TableName = TableName;
            paramList = new Dictionary<string, object>();
            conditionList = new Dictionary<string, object>();
        }

        public BaseSqlGenerator()
        {

        }

        protected object PrepareParam(object Param)
        {
            if (Param.ToString().StartsWith("@"))
                return Param;

            if (Param is String)
            {
                return $"'{Param}'";
            }

            return Param;
        }

        public virtual String Generate()
        {
            if (String.IsNullOrEmpty(TableName))
                throw new Exception("Table name can't be empty");

            return "";
        }

        protected virtual String GenerateParams()
        {
            return String.Join(",", paramList.Select(i => i.Key + "=" + i.Value));
        }

        protected virtual String GenerateConditions()
        {
            return String.Join(" AND ", conditionList.Select(i => i.Key + "=" + PrepareParam(i.Value)));
        }

        public void Dispose()
        {
            paramList = null;
            conditionList = null;
        }

        public void AddParam(String FieldName, object Value)
        {
            paramList.Add(FieldName, Value);
        }

        public void AddCondition(String FieldName, object Value)
        {
            conditionList.Add(FieldName, Value);
        }

        public override string ToString()
        {
            return Generate();
        }
    }
}