﻿using CourierServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CourierServices.DVO;
using ServiceDvo.Accounts;

namespace CourierServices.Base
{
    public abstract class BaseSms : IBaseSms
    {
        public BaseSms()
        {
        }

        public String PhoneNumber { get; set; }

        public String Content { get; set; }

        public abstract SmsResponse Send();

        public SmsAccount Account { get; set; }
        public string Message { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public object CustomSettings { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}