﻿using ServiceDvo.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Base
{
    public abstract class BaseSmsProfile
    {
        public Guid Guid { get; set; }

        public String Name { get; set; }

        public bool IsActive { get; set; }

        public SmsAccount Account { get; set; }
    }
}