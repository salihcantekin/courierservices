﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Base
{
    public abstract class BaseDVO
    {
        protected Type BaseType { get; set; }

        public BaseDVO(object Value)
        {
            Load(Value);
        }

        public BaseDVO()
        {

        }

        public virtual void Load(object Value)
        {
            if (BaseType == null)
                return;

            //if (!Value.GetType().Equals(BaseType.GetType()))
            //    throw new InvalidCastException($"Value must be {BaseType.GetType().Name}");
        }
    }
}