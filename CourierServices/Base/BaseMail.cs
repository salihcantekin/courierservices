﻿using CourierServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CourierServices.Enums;
using CourierServices.Profiles;
using System.Text;
using CourierServices.Attachment;
using CourierServices.Collections;
using ServiceDvo.Profiles;
using ServiceDvo.Accounts;

namespace CourierServices.Base
{
    public abstract class BaseMail : IBaseMail
    {
        public AttachmentCollection Attachments;

        public BaseMail()
        {
            Attachments = new AttachmentCollection();
        }

        public abstract void ChangeStatus(MailStatus Status);

        public abstract void Send();


        public Guid Guid { get; set; }

        public MailProfile MailProfile { get; set; }

        public MailAccount MailAccount { get; set; }

        public String Subject { get; set; }

        public String MailBody { get; set; }

        public List<String> ToAddresses { get; set; }
        public List<String> CcAddresses { get; set; }
        public List<String> BccAddresses { get; set; }

        public bool IsHtmlContent { get; set; }

        public Encoding BodyEncoding { get; set; }

        public Encoding SubjectEncoding { get; set; }

    }
}