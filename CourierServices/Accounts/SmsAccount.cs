﻿using CourierServices.Base;
using CourierServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Accounts
{
    public abstract class SmsAccount : BaseSmsAccount
    {
        public abstract int Delete();

        public abstract int Insert();

        public abstract void Save();

        public abstract int Update();

        public abstract String GetTableName();
    }
}