﻿using CourierServices.Base;
using CourierServices.CustomSettings;
using CourierServices.Interfaces;
using CourierServices.SqlGenerator;
using DBManager;
using DBManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Accounts
{
    public class MobilDevSmsAccount : BaseSmsAccount, IDBBase
    {
        public override int Delete()
        {
            using (DeleteSqlGenerator generator = new DeleteSqlGenerator(GetTableName()))
            {
                generator.AddCondition("GUID", Guid.ToString());

                using (DbTran tran = new DbTran())
                {
                    return tran.Provider.ExecuteNonQuery(generator.Generate());
                }
            }
        }

        public override string GetTableName()
        {
            return "SMS_ACCOUNT";
        }


        public override object GetCustomSettings()
        {
            return Settings;
        }

        public override void Load()
        {
            String Sql = $"SELECT * FROM {GetTableName()}";

            using (DbTran db = new DbTran())
            {
                IDbDataReader dr = db.Provider.ExecuteReader(Sql);

                if (dr.Read())
                    LoadFromReader(dr);
            }
        }

        public override void Save()
        {
            if (Guid == null || Guid == Guid.Empty)
                Insert();
            else
                Update();
        }

        public override int Insert()
        {
            using (InsertSqlGenerator generator = new InsertSqlGenerator(GetTableName()))
            {
                Guid = Guid.NewGuid();
                generator.AddParam("GUID", Guid.ToString());
                generator.AddParam("IsActive", IsActive ? "A" : "P");
                generator.AddParam("Name", Name);


                using (DbTran tran = new DbTran())
                {
                    return tran.Provider.ExecuteNonQuery(generator.Generate());
                }
            }
        }

        public override int Update()
        {
            using (UpdateSqlGenerator generator = new UpdateSqlGenerator(GetTableName()))
            {
                generator.AddCondition("Guid", Guid.ToString());
                generator.AddParam("IsActive", IsActive ? "A" : "P");
                generator.AddParam("Name", Name);

                using (DbTran db = new DbTran())
                    return db.Provider.ExecuteNonQuery(generator.Generate());
            }
        }

        private void LoadFromReader(IDbDataReader dr)
        {
            Guid = dr.GetValue<Guid>("GUID");
            Name = "MobilDev1";
            IsActive = true;

            MobilDevSettings settings = new MobilDevSettings
            {
                RootUrl = dr.GetString("ROOT_URL"),
                ParameterList = new Dictionary<string, object>()
                {
                    ["key"] = dr.GetString("KEY"),
                    ["secret"] = dr.GetString("SCRET"),
                    ["originator"] = dr.GetString("DEFAULTORIGINATOR"),
                    ["msisdn"] = dr.GetString("MSI_SDN"),
                    ["blacklist"] = dr.GetInt("BLACKLIST").ToString(),
                    ["channel"] = dr.GetString("CHANNEL"),
                    ["sendType"] = "1",
                    ["language"] = "turkish"
                }
            };

            Settings = settings;
        }
    }
}