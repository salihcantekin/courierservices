﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CourierServices
{
    public class Logger
    {
        private static Logger logger;
        public static Logger Default => logger ?? (logger = new Logger());


        public void WriteToFile(String logStr)
        {
            String filePath = "C:\\Logs\\Log.txt";
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(logStr);
            }
        }
    }
}