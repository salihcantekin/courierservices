﻿using DBManager;
using DBManager.Parameters;
using ServiceDvo;
using ServiceDvo.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Helpers.DbHelper
{
    public class MailAccountHelper
    {
        private static MailAccountHelper _default;
        public static MailAccountHelper Default => _default ?? (_default = new MailAccountHelper());

        public void Save(MailAccount mailAccount)
        {
            if (mailAccount.Guid != null && mailAccount.Guid != Guid.Empty)
                Update(mailAccount);
            else
                Insert(mailAccount);
        }

        private int Update(MailAccount mailAccount)
        {
            String Sql = $@"UPDATE {GetTableName()} a
						   SET a.name = @Name,
							   a.host_address = @Host_address,
							   a.smtp_port = @Smtp_port,
							   a.username = @Username,
							   a.user_password = @User_password,
							   a.sender_mail = @Sender_mail,
							   a.use_ssl = @Use_ssl,
							   a.charset = @Charset,
							   a.noreply = @Noreply
						 WHERE GUID = @GUID";

            using (DbTran tran = new DbTran())
            {
                int Result = tran.Provider.ExecuteNonQuery(Sql, DBParameterCollection.AddOnlyOne("GUID", mailAccount.Guid.ToString()));
                return Result;
            }
        }

        public int Delete(MailAccount mailAccount)
        {
            String Sql = $"DELETE FROM {GetTableName()} WHERE GUID = @GUID";
            int Result = 0;

            using (DbTran tran = new DbTran())
                Result = tran.Provider.ExecuteNonQuery(Sql, DBParameterCollection.AddOnlyOne("GUID", mailAccount.Guid.ToString()));

            return Result;
        }

        private DBParameterCollection GetFixParametersWithOrder(MailAccount mailAccount)
        {
            DBParameterCollection Coll = new DBParameterCollection
            {
                { "Name", mailAccount.Name },
                { "Host_address", mailAccount.HostName },
                { "Smtp_port", mailAccount.Port },
                { "Username", mailAccount.UserAuth?.UserName },
                { "User_password", mailAccount.UserAuth?.Password },
                { "Sender_mail", mailAccount.SenderMail },
                { "Use_ssl", mailAccount.UseSsl ? "Y" : "N" },
                { "Charset", mailAccount.Charset },
                { "Noreply", mailAccount.NoReplyMail }
            };
            return Coll;
        }

        public int Insert(MailAccount mailAccount)
        {
            String Sql = $@"INSERT INTO {GetTableName()} a (a.recid,
																a.name,
																a.host_address,
																a.smtp_port,
																a.username,
																a.user_password,
																a.sender_mail,
																a.use_ssl,
																a.charset,
																a.noreply)
								 VALUES (@Guid,
										 @Name,
										 @Host_address,
										 @Smtp_port,
										 @Username,
										 @User_password,
										 @Sender_mail,
										 @Use_ssl,
										 @Charset,
										 @Noreply)";

            using (DbTran tran = new DbTran())
            {
                DBParameterCollection Coll = GetFixParametersWithOrder(mailAccount);
                mailAccount.Guid = new Guid();
                Coll.Insert(0, new DBParameter() { Name = "Guid", Value = mailAccount.Guid.ToString() });
                int Result = tran.Provider.ExecuteNonQuery(Sql, Coll);
                Coll = null;
                return Result;
            }
        }

        public string GetTableName()
        {
            return "mail_accounts";
        }
    }
}