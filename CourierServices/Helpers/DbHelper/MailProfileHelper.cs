﻿using CourierServices.SqlGenerator;
using DBManager;
using DBManager.Parameters;
using ServiceDvo.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Helpers.DbHelper
{
    public class MailProfileHelper
    {
        private static MailProfileHelper _default;
        public static MailProfileHelper Default => _default ?? (_default = new MailProfileHelper());


        public int Delete(MailProfile mailProfile)
        {
            using (DeleteSqlGenerator generator = new DeleteSqlGenerator(GetTableName()))
            {
                generator.AddCondition("GUID", mailProfile.Guid.ToString());

                using (DbTran db = new DbTran())
                    return db.Provider.ExecuteNonQuery(generator.Generate());

            }
        }

        private int Insert(MailProfile mailProfile)
        {
            using (DbTran db = new DbTran())
            {
                using (InsertSqlGenerator generator = new InsertSqlGenerator(GetTableName()))
                {
                    generator.AddParam("GUID", "@GUID");
                    generator.AddParam("PROFILE_NAME", "@ProfileName");
                    generator.AddParam("SENDER_DISPLAY_NAME", "@SenderDisplayName");
                    generator.AddParam("STATUS", "@Status");

                    DBParameterCollection Coll = this.GetFixParameterWithOrder(mailProfile);
                    mailProfile.Guid = Guid.NewGuid();
                    Coll.Insert(0, new DBParameter() { Name = "GUID", Value = mailProfile.Guid.ToString() });

                    return db.Provider.ExecuteNonQuery(generator.Generate(), Coll);
                }
            }
        }

        public void Save(MailProfile mailProfile)
        {
            if (mailProfile.Guid != null && mailProfile.Guid != Guid.Empty)
                Update(mailProfile);
            else
                Insert(mailProfile);
        }

        private int Update(MailProfile mailProfile)
        {
            int Result = 0;

            using (UpdateSqlGenerator generator = new UpdateSqlGenerator(GetTableName()))
            {
                generator.AddParam("PROFILE_NAME", "@ProfileName");
                generator.AddParam("SENDER_DISPLAY_NAME", "@SenderDisplayName");
                generator.AddParam("STATUS", "@Status");
                generator.AddCondition("GUID", "@GUID");
                String Sql = generator.Generate();

                DBParameterCollection Coll = GetFixParameterWithOrder(mailProfile);
                Coll.Add("GUID", mailProfile.Guid.ToString());

                using (DbTran db = new DbTran())
                {
                    Result = db.Provider.ExecuteNonQuery(Sql, Coll);
                }
            }

            return Result;
        }

        private DBParameterCollection GetFixParameterWithOrder(MailProfile mailProfile)
        {
            DBParameterCollection Coll = new DBParameterCollection();
            Coll.Add("ProfileName", mailProfile.Name);
            Coll.Add("SenderDisplayName", mailProfile.SenderDisplayName);
            Coll.Add("Status", mailProfile.IsActive ? "A" : "P");

            return Coll;
        }

        private string GetTableName()
        {
            return "MAIL_PROFILES";
        }
    }
}