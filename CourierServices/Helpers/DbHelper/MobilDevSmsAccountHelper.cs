﻿using CourierServices.SqlGenerator;
using DBManager;
using DBManager.Interfaces;
using ServiceDvo.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.Helpers.DbHelper
{
    public class MobilDevSmsAccountHelper
    {
        private static MobilDevSmsAccountHelper _default;
        public static MobilDevSmsAccountHelper Default => _default ?? (_default = new MobilDevSmsAccountHelper());


        public int Delete(MobilDevSmsAccount smsAccount)
        {
            using (DeleteSqlGenerator generator = new DeleteSqlGenerator(GetTableName()))
            {
                generator.AddCondition("GUID", smsAccount.Guid.ToString());

                using (DbTran tran = new DbTran())
                {
                    return tran.Provider.ExecuteNonQuery(generator.Generate());
                }
            }
        }

        public string GetTableName()
        {
            return "SMS_ACCOUNT";
        }


        public object GetCustomSettings(SmsAccount smsAccount)
        {
            return smsAccount.CustomSettings;
        }



        //public void Load()
        //{
        //    String Sql = $"SELECT * FROM {GetTableName()}";

        //    using (DbTran db = new DbTran())
        //    {
        //        IDbDataReader dr = db.Provider.ExecuteReader(Sql);

        //        if (dr.Read())
        //            LoadFromReader(dr);
        //    }
        //}

        public void Save(MobilDevSmsAccount smsAccount)
        {
            if (smsAccount.Guid == null || smsAccount.Guid == Guid.Empty)
                Insert(smsAccount);
            else
                Update(smsAccount);
        }

        public int Insert(MobilDevSmsAccount smsAccount)
        {
            using (InsertSqlGenerator generator = new InsertSqlGenerator(GetTableName()))
            {
                smsAccount.Guid = Guid.NewGuid();
                generator.AddParam("GUID", smsAccount.Guid.ToString());
                //generator.AddParam("IsActive", smsAccount.IsActive ? "A" : "P");
                //generator.AddParam("Name", smsAccount.Name);


                using (DbTran tran = new DbTran())
                    return tran.Provider.ExecuteNonQuery(generator.Generate());
            }
        }

        public int Update(MobilDevSmsAccount smsAccount)
        {
            using (UpdateSqlGenerator generator = new UpdateSqlGenerator(GetTableName()))
            {
                generator.AddCondition("Guid", smsAccount.Guid.ToString());
                //generator.AddParam("IsActive", smsAccount.IsActive ? "A" : "P");
                //generator.AddParam("Name", smsAccount.Name);

                using (DbTran db = new DbTran())
                    return db.Provider.ExecuteNonQuery(generator.Generate());
            }
        }

        //private void LoadFromReader(IDbDataReader dr)
        //{
        //    Guid = dr.GetValue<Guid>("GUID");
        //    Name = "MobilDev1";
        //    IsActive = true;

        //    MobilDevSettings settings = new MobilDevSettings
        //    {
        //        RootUrl = dr.GetString("ROOT_URL"),
        //        ParameterList = new Dictionary<string, object>()
        //        {
        //            ["key"] = dr.GetString("KEY"),
        //            ["secret"] = dr.GetString("SCRET"),
        //            ["originator"] = dr.GetString("DEFAULTORIGINATOR"),
        //            ["msisdn"] = dr.GetString("MSI_SDN"),
        //            ["blacklist"] = dr.GetInt("BLACKLIST").ToString(),
        //            ["channel"] = dr.GetString("CHANNEL"),
        //            ["sendType"] = "1",
        //            ["language"] = "turkish"
        //        }
        //    };

        //    Settings = settings;
        //}

    }
}