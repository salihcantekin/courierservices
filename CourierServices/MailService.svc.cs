﻿using CourierServices.Base;
using CourierServices.DVO;

using ServiceDvo;
using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using ServiceDvo.Result;
using System;
using System.Collections.Generic;

namespace CourierServices
{
    public class MailService : IMailService
    {
        public Result<List<MailProfile>> GetProfileDVOList()
        {
            Result<List<MailProfile>> Result = new Result<List<MailProfile>>();

            try
            {
                Result.Value = DefinitionService.Service.GetMailProfiles();
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public Result<MailResult> Send(MailDvo mailDvo)
        {
            Result<MailResult> Result = new Result<MailResult>();
            try
            {
                Result.Value = SenderService.Service.SendMail(mailDvo);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }
            
            return Result;
        }


        #region MailProfile Operations

        public BaseResult SaveMailProfile(MailProfile Profile)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.Save(Profile);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public Result<List<MailProfile>> GetMailProfiles()
        {
            Result<List<MailProfile>> Result = new Result<List<MailProfile>>();
            try
            {
                Result.Value = DefinitionService.Service.GetMailProfiles();
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public BaseResult DeleteMailProfile(Guid ProfileId)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.DeleteMailProfile(ProfileId);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        #endregion

        #region MailAccount Operations

        public BaseResult SaveMailAccount(MailAccount Account)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.Save(Account);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public Result<List<MailAccount>> GetMailAccounts()
        {
            Result<List<MailAccount>> Result = new Result<List<MailAccount>>();
            try
            {
                Result.Value = DefinitionService.Service.GetMailAccounts();
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public BaseResult DeleteMailAccount(Guid AccountId)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.DeleteMailAccount(AccountId);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        #endregion
    }
}
