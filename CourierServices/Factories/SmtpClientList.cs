﻿using CourierServices.Profiles;
using ServiceDvo.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace CourierServices.Factories
{
    public class SmtpClientList
    {
        private static SmtpClientList current;
        public static SmtpClientList Current => current ?? (current = new SmtpClientList());

        public SmtpClient CreateByProfile(MailProfile Profile)
        {
            SmtpClient Client = new SmtpClient();
            SetSmtpSettings(Client, Profile);
            return Client;
        }

        private void SetSmtpSettings(SmtpClient Client, MailProfile Profile)
        {
            Client.Host = Profile.MailAccount.HostName;
            Client.Port = Profile.MailAccount.Port;
            Client.EnableSsl = Profile.MailAccount.UseSsl;
            Client.DeliveryMethod = SmtpDeliveryMethod.Network;
            Client.UseDefaultCredentials = Profile.MailAccount.UseAuthentication;
            if (Profile.MailAccount.UseAuthentication)
                Client.Credentials = new System.Net.NetworkCredential(Profile.MailAccount.UserAuth.UserName, Profile.MailAccount.UserAuth.Password);
        }
    }
}