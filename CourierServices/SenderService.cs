﻿using CourierLib;
using CourierLib.Business;
using CourierServices.Base;
using CourierServices.Collections;
using CourierServices.CustomSettings;
using CourierServices.DVO;
using CourierServices.Mails;
using CourierServices.Profiles;
using CourierServices.Utilities;
using DBManager;
using DBManager.Parameters;
using DBManager.SqlGenerator;
using Newtonsoft.Json.Linq;
using ServiceDvo;
using ServiceDvo.Accounts;
using ServiceDvo.Request;
using ServiceDvo.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CourierServices
{
    public class SenderService
    {
        private static SenderService service;


        public static SenderService Service => service ?? (service = new SenderService());

        internal MailResponse SendEuroMessageMail(Guid ProfileId)
        {
            MailResponse Result = new MailResponse();

            RequestDvo dvo = new RequestDvo
            {
                Url = "http://api.relateddigital.com/reste/api/auth/login",
                Method = "POST",
                ContentType = "application/json",
                Content = "{ \"UserName\" : \"etstur_wsuser_ecomm\", \"Password\" :\"e+$TuR#pa$s\" }"
            };

            RequestResult requestResult = RequestBusiness.ExecuteRequest(dvo);

            if (requestResult.IsSuccess)
            {
                Logger.Default.WriteToFile("It's Successful");

                JToken token = JObject.Parse(requestResult.ResponseString);
                if (token.SelectToken("Success") == null)
                    throw new Exception(token.SelectToken("error").ToString());

                Result.Message = token.SelectToken("Message")?.ToString();

                String serviceTicket = token.SelectToken("ServiceTicket")?.ToString();

                EuroMessageMail mail = new EuroMessageMail()
                {
                    FromName = "Etstur",
                    FromAddress = "anket@e.etstur.com",
                    ReplyAddress = "noreply@e.etstur.com",
                    Subject = "Test",
                    Charset = "iso-8859-9",
                    ToName = "Salih Cantekin",
                    ToEmailAddress = "salihcantekin@gmail.com",
                    HtmlBody = "<html> <head></head> <body><p> Değerli Yetkilimiz, </p> Misafirimizin iptal onayı Acentemize mail ile iletilmiştir. Konunun takibi bilgilerinize sunulur.</br><p>Saygılarımızla</p><p>Etstur</p></body> </html>\","
                };

                dvo = new RequestDvo
                {
                    Url = "http://api.relateddigital.com/reste/api/post/PostHtml",
                    Method = "POST",
                    ContentType = "application/json",
                    Content = Utils.JsonToString(mail)
                };

                dvo.HeaderList.Add("Authorization", serviceTicket);

                requestResult = RequestBusiness.ExecuteRequest(dvo);
                if (requestResult == null)
                    Logger.Default.WriteToFile("It's null");

                if (requestResult.IsSuccess)
                {

                }
                else
                    throw new Exception(requestResult.ResponseString);
            }
            else
                throw new Exception(requestResult.ResponseString);

            return Result;

            //return null;

            //MailResponse Result = new MailResponse();

            //BaseMail Mail = new EuroMessageMail
            //{
            //    MailProfile = Constants.MailProfileCollection.Values.FirstOrDefault(i => i.Guid.Equals(ProfileId)),
            //    ToAddresses = new List<String>() { "salihcantekin@gmail.com" }
            //};

            //Mail.Attachments.Add(new Attachment.Attachment()
            //{
            //    AttType = Enums.AttachmentType.DownloadFromUrl,
            //    FullFilePath = "https://imagessl.etstur.com/files/images/site/images/gemi/etsGemi2018/EtsGemisi-Home.jpg",
            //    FileName = "TestAttch001"
            //});

            //Mail.MailBody = "TEST TEST TEST";
            //Mail.Subject = "Subject";

            //Mail.Send();

            //return Result;
        }


        internal MailResult SendMail(MailDvo mailDVO)
        {
            if (mailDVO.Guid == null || mailDVO.Guid == Guid.Empty)
                mailDVO.Guid = Guid.NewGuid();

            MailResult result = new MailResult();

            MailBusiness.SendMail(mailDVO);

            result.Id = mailDVO.Guid;
            return result;
        }

        internal MailResult AddMailToQueue(MailDvo mailDVO)
        {
            if (mailDVO.Guid == null || mailDVO.Guid == Guid.Empty)
                mailDVO.Guid = Guid.NewGuid();

            MailResult result = new MailResult();

            #region Add to database

            //InsertSqlGenerator sql = new InsertSqlGenerator
            //{
            //    TableName = "MAIL_QUEUE"
            //};

            //sql.AddFieldWithoutParameter("ID", "MAIL_QUEUE_SEQ.NEXTVAL");
            //sql.AddField("GUID", mailDVO.Guid.ToString());
            //sql.AddField("SUBJECT", mailDVO.Subject);
            //sql.AddField("MAIL_BODY", mailDVO.MailBody);
            //sql.AddField("MAIL_TO", String.Join(";", mailDVO.To));

            //if (mailDVO.CC != null)
            //    sql.AddField("MAIL_CC", String.Join(";", mailDVO.CC));

            //if (mailDVO.Bcc != null)
            //    sql.AddField("MAIL_BCC", String.Join(";", mailDVO.Bcc));

            //sql.AddField("DISPLAY_NAME", mailDVO.SenderDisplayName);
            //sql.AddField("ACCOUNT_GUID", mailDVO.Account.Guid.ToString());
            //sql.AddField("ISHTML", mailDVO.IsHtmlBody ? "Y" : "N");
            //sql.AddField("MAIL_PRIORITY", (int)mailDVO.Priority);
            //sql.AddField("ENCODING", mailDVO.Encoding.ToString());
            //sql.AddField("MAIL_STATUS", 0); // Waiting

            //String sqlText = sql.PrepareSql();

            //using (DbTran tran = new DbTran())
            //{
            //    DBParameterCollection coll = new DBParameterCollection();
            //    coll.AddRange(sql.FieldList.Select(i => i.Parameter));

            //    int resultDb = tran.Provider.ExecuteNonQuery(sqlText, coll);
            //    if (resultDb <= 0)
            //        throw new Exception("Error occured when adding to queue in DB");

            //    tran.Provider.CommitTransaction();
            //}

            #endregion

            MailBusiness.AddQueue(mailDVO);

            result.Id = mailDVO.Guid;
            return result;
        }


        internal SmsResult SendMobilDevSms(Guid AccountId, String PhoneNumber, String Message)
        {
            MobilDevSmsDvo dvo = new MobilDevSmsDvo();

            MobilDevSmsAccount smsAccount = Constants.MobilDevSmsAccountCollection.FindValue(AccountId);
            if (smsAccount != null)
            {
                dvo.Url = smsAccount.Url;
                dvo.Key = smsAccount.Key;
                dvo.Secret = smsAccount.Secret;
                dvo.Originator = smsAccount.Originator;
                dvo.MsiSdn = smsAccount.MsiSdn;
                dvo.BlackList = smsAccount.BlackList;
                dvo.Channel = smsAccount.Channel;
                dvo.SendType = smsAccount.SendType;
                dvo.Language = smsAccount.Language;

                dvo.Message = Message;
                dvo.PhoneNumber = PhoneNumber;
            }

            return SendMobilDevSms(dvo);
        }

        internal SmsResult SendMobilDevSms(MobilDevSmsDvo smsDvo)
        {
            SmsResult Result = new SmsResult();

            try
            {
                using (RequestDvo dvo = new RequestDvo())
                {
                    using (UrlBuilder url = new UrlBuilder(smsDvo.Url))
                    {
                        url.Add("key", smsDvo.Key);
                        url.Add("secret", smsDvo.Secret);
                        url.Add("originator", smsDvo.Originator);
                        url.Add("msisdn", smsDvo.MsiSdn);
                        url.Add("blacklist", smsDvo.BlackList);
                        url.Add("channel", smsDvo.Channel);
                        url.Add("sendType", smsDvo.SendType);
                        url.Add("language", smsDvo.Language);
                        dvo.Url = url.Build();
                    }

                    smsDvo.Message = smsDvo.Message.Replace(Environment.NewLine, "&#10;");
                    dvo.Content = $"{smsDvo.PhoneNumber} {smsDvo.Message}";
                    dvo.Method = "POST";
                    dvo.ContentType = "raw";

                    RequestResult requestResult = RequestBusiness.ExecuteRequest(dvo);

                    if (!requestResult.IsSuccess)
                        throw new Exception(requestResult.ErrorMessage);

                    JToken token = JObject.Parse(requestResult.ResponseString);
                    Result.IsSuccess = token.SelectToken("error") == null;

                    if (token.SelectToken("Message") != null)
                    {
                        Result.Message = token.SelectToken("Message").ToString();
                    }

                    if (token.SelectToken("queueId") != null)
                        Result.UniqueId = token.SelectToken("queueId").ToString();
                    token = null;
                }
            }
            catch (Exception ex)
            {
                Result.SetError(ex);
            }

            return Result;
        }
    }
}