﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.CustomExceptions
{
    public class NotFoundInListException : Exception
    {
        public NotFoundInListException(String Message) : base(Message)
        {

        }

        public NotFoundInListException(Exception Ex) : this(Ex.Message)
        {

        }
    }
}