﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices.CustomExceptions
{
    public class ChildNotFoundException: Exception
    {
        public ChildNotFoundException(String Message): base(Message)
        {
            
        }

        public ChildNotFoundException(Exception Ex): this(Ex.Message)
        {

        }
    }
}