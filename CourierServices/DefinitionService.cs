﻿using CourierServices.Base;
using CourierServices.Collections;
using CourierServices.DVO;
using CourierServices.Helpers.DbHelper;
using CourierServices.Profiles;
using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierServices
{
    public class DefinitionService
    {
        private static DefinitionService service;
        public static DefinitionService Service => service ?? (service = new DefinitionService());

        #region MailProfile Operations

        public List<MailProfile> GetMailProfiles()
        {
            return Constants.MailProfileCollection.ToList();
        }

        public void Save(MailProfile Profile)
        {
            MailProfileHelper.Default.Save(Profile);
        }

        public int DeleteMailProfile(Guid ProfileId)
        {
            MailProfile profile = Constants.MailProfileCollection.FindValue(ProfileId);
            if (profile == null)
                throw new KeyNotFoundException("Profile not found");

            int Result = MailProfileHelper.Default.Delete(profile);
            Constants.MailProfileCollection.Remove(profile.Guid);

            return Result;
        }

        #endregion

        #region MailAccount Operations

        public List<MailAccount> GetMailAccounts()
        {
            return Constants.MailAccountCollection.ToList();
        }

        public void Save(MailAccount Account)
        {
            MailAccountHelper.Default.Save(Account);
        }

        public int DeleteMailAccount(Guid AccountId)
        {
            MailAccount account = Constants.MailAccountCollection.FindValue(AccountId);
            if (account == null)
                throw new KeyNotFoundException("Account not found");

            int Result = MailAccountHelper.Default.Delete(account);

            Constants.MailAccountCollection.Remove(account.Guid);
            return Result;
        }

        #endregion

        #region SmsProfile Operations

        //public List<SmsProfileDVO> GetSmsProfiles()
        //{
        //    return Constants.SmsProfileCollection.
        //               Select(i => new SmsProfileDVO(i.Value)).
        //               ToList();
        //}

        //public int DeleteSmsProfile(Guid ProfileGuid)
        //{
        //    SmsProfile Profile = Constants.SmsProfileCollection.FindValue(ProfileGuid);
        //    int Result = Profile.Delete();
        //    Constants.SmsProfileCollection.Remove(Profile.Guid);
        //    return Result;
        //}

        public void Save(SmsProfile Profile)
        {
            Profile.Save();
        }

        #endregion

        #region SmsAccount Operations

        public List<SmsAccountDVO> GetSmsAccounts()
        {
            return Constants.MobilDevSmsAccountCollection.
                    Select(i => new SmsAccountDVO(i.Value)).
                    ToList();
        }

        public int DeleteSmsAccount(Guid AccountId)
        {
            MobilDevSmsAccount Account = Constants.MobilDevSmsAccountCollection.FindValue(AccountId);
            int Result = MobilDevSmsAccountHelper.Default.Delete(Account);
            Constants.MobilDevSmsAccountCollection.Remove(Account.Guid);
            return Result;
        }

        public void Save(BaseSmsAccount Account)
        {
            Account.Save();
        }

        #endregion
    }
}