﻿using CourierServices.Base;
using CourierServices.Collections;
using CourierServices.DVO;
using CourierServices.Interfaces;
using CourierServices.Profiles;

using ServiceDvo.Accounts;
using ServiceDvo.Profiles;
using ServiceDvo.Result;
using System;
using System.Collections.Generic;

namespace CourierServices
{
    public class DefinitionsService : IDefinitionService
    {
        #region SmsProfile Operations

        public BaseResult SaveSmsProfile(SmsProfile Profile)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.Save(Profile);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        //public Result<List<SmsProfileDVO>> GetSmsProfiles()
        //{
        //    Result<List<SmsProfileDVO>> Result = new Result<List<SmsProfileDVO>>();

        //    try
        //    {
        //        Result.Value = DefinitionService.Service.GetSmsProfiles();
        //        Result.IsSuccess = true;
        //    }
        //    catch (Exception Ex)
        //    {
        //        Result.SetError(Ex);
        //    }


        //    return Result;
        //}

        //public BaseResult DeleteSmsProfile(Guid ProfileId)
        //{
        //    BaseResult Result = new BaseResult();
        //    try
        //    {
        //        DefinitionService.Service.DeleteSmsProfile(ProfileId);
        //        Result.IsSuccess = true;
        //    }
        //    catch (Exception Ex)
        //    {
        //        Result.SetError(Ex);
        //    }

        //    return Result;
        //}

        #endregion

        #region SmsAccount Operations
        
        public BaseResult SaveSmsAccount(BaseSmsAccount Profile)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.Save(Profile);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public Result<List<SmsAccountDVO>> GetSmsAccounts()
        {
            Result<List<SmsAccountDVO>> Result = new Result<List<SmsAccountDVO>>();
            try
            {
                Result.Value = DefinitionService.Service.GetSmsAccounts();
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public BaseResult DeleteSmsAccount(Guid AccountId)
        {
            BaseResult Result = new BaseResult();
            try
            {
                DefinitionService.Service.DeleteSmsAccount(AccountId);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }

        public BaseResult SaveSmsAccount(SmsAccountDVO Account)
        {
            throw new NotImplementedException();
        }
        

        #endregion
    }
}
