﻿using CourierServices.Base;
using CourierServices.DVO;
using ServiceDvo.Result;
using System;

namespace CourierServices
{
    public class SmsService : ISmsService
    {
        public Result<SmsResult> Send(Guid AccountId, String PhoneNumber, String Content)
        {
            Result<SmsResult> Result = new Result<SmsResult>();
            try
            {
                Result.Value = SenderService.Service.SendMobilDevSms(AccountId, PhoneNumber, Content);
                Result.IsSuccess = true;
            }
            catch (Exception Ex)
            {
                Result.SetError(Ex);
            }

            return Result;
        }
    }
}
