﻿using CourierLib.Events;
using CourierLib.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CourierLib.Base
{
    // Kısır, Çay ve Armutlu Soda
    public abstract class BaseQueue<T> : IQueue<T>, IBaseQueue, IDisposable, INotifyPropertyChanged
    {
        #region Encapsulated Fields

        private QueueState state;
        public QueueState State
        {
            get => state;
            set
            {
                if (state != value)
                {
                    state = value;
                    propertyChanged("State");
                    OnQueueStateChanged?.Invoke(this, new OnQueueStateChanged(value));
                }
            }
        }

        private bool async;
        public bool Async { get => async; protected set => async = value; }

        #endregion

        #region Private Fields

        private HashSet<T> list;
        private Task mainTask;
        private ManualResetEvent shutdownEvent;
        private ManualResetEvent pauseEvent;

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<OnWorkingOnEventArgs<T>> OnWorkingOn;
        public event EventHandler<OnWorkingCompleteEventArgs<T>> OnWorkingComplete;
        public event EventHandler<OnQueueStateChanged> OnQueueStateChanged;

        #endregion

        #region Constructor

        public BaseQueue()
        {
            shutdownEvent = new ManualResetEvent(false);
            pauseEvent = new ManualResetEvent(true);

            mainTask = new Task(() => doWork());
            State = QueueState.WaitingForActivation;

            list = new HashSet<T>();
        }

        #endregion

        #region Private Methods

        private void doWork()
        {
            while (true)
            {
                pauseEvent.WaitOne(Timeout.Infinite);

                if (shutdownEvent.WaitOne(0))
                    break;

                if (list.Count == 0)
                {
                    Pause();
                    continue;
                }

                State = QueueState.Executing;

                T value = list.First();

                OnWorkingCompleteEventArgs<T> workingComlete = new OnWorkingCompleteEventArgs<T>(value);

                try
                {
                    object result = null;

                    if (Async)
                    {
                        BackgroundWorker worker = new BackgroundWorker();
                        worker.DoWork += worker_DoWork;
                        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                        worker.RunWorkerAsync(value);
                    }
                    else
                    {
                        result = WorkingOn(value);

                        workingComlete.Result = result;
                        workingComlete.Value = value;
                    }

                    OnWorkingOn?.Invoke(this, new OnWorkingOnEventArgs<T>(value, result));
                }
                catch (Exception Ex)
                {
                    workingComlete.IsSuccess = false;
                    workingComlete.Message = Ex.Message;
                    workingComlete.MessageDetail = Ex.ToString();
                }
                finally
                {
                    OnWorkingComplete?.Invoke(this, workingComlete);
                }

                Remove(value);
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnWorkingComplete == null)
                return;

            OnWorkingCompleteEventArgs<T> workingComplete = new OnWorkingCompleteEventArgs<T>();

            if (e.Error != null)
            {
                workingComplete.IsSuccess = false;
                workingComplete.Message = e.Error.Message;
                workingComplete.MessageDetail = e.Error.ToString();
            }
            else
            {
                workingComplete.IsSuccess = true;
                workingComplete.Value = (T)e.Result;
                workingComplete.Result = e.Result;
            }

            OnWorkingComplete?.Invoke(this, workingComplete);
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = WorkingOn((T)e.Argument);
        }

        private void propertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Public Abstract Methods

        public abstract object WorkingOn(T value);

        public abstract bool Validate(T value);

        #endregion

        #region Public Methods

        public void Dispose()
        {
            Stop();
        }

        public void BeginUpdate()
        {
            if (State == QueueState.Paused)
                return;

            Pause();
        }

        public void EndUpdate()
        {
            if (State != QueueState.WaitingForRun)
                return;

            Resume();
        }

        public void Pause()
        {
            State = QueueState.WaitingForRun;
            pauseEvent.Reset();
        }

        public void Resume()
        {
            pauseEvent.Set();
        }

        public void ReStart()
        {
            Stop();
            Start();
        }

        public void Start()
        {
            if (mainTask.Status <= TaskStatus.Created)
                mainTask.Start();

            shutdownEvent.Reset();

            State = QueueState.Started;
        }

        public void Stop()
        {
            State = QueueState.Stopped;
            pauseEvent.Set();
            shutdownEvent.Set();
        }

        public void Add(T value)
        {
            if (!Validate(value))
                return;

            list.Add(value);

            if (State == QueueState.WaitingForActivation || State == QueueState.WaitingForRun)
                Start();
            else if (State == QueueState.Paused)
                Resume();
        }

        public void AddRange(ICollection<T> value)
        {
            BeginUpdate();

            foreach (T item in value)
                Add(item);

            EndUpdate();
        }

        public void Remove(T value)
        {
            list.Remove(value);
        }

        #endregion
    }
}
