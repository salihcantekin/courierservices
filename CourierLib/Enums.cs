﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib
{
    public enum QueueState
    {
        Stopped = 1,
        Executing = 2,
        WaitingForActivation = 4,
        WaitingForRun = 8,
        Started = 16,
        Paused = 32
    }

    public enum MailPriority
    {
        High = 0,
        Normal = 1,
        Low = 2
    }
}
