﻿using CourierLib.Interfaces;
using CourierLib.Queue;
using ServiceDvo;
using ServiceDvo.Request;
using System.Collections.Generic;
using System.Linq;

namespace CourierLib.Business
{
    public class QueueManager
    {
        private static MailQueue mailQueue;
        private static DbExecuteSqlQueue dbExecuteQueue;
        private static RequestQueue requestQueue;

        private static List<IBaseQueue> queueList;

        static QueueManager()
        {
            mailQueue = new MailQueue();
            dbExecuteQueue = new DbExecuteSqlQueue();
            requestQueue = new RequestQueue();

            queueList = new List<IBaseQueue>()
            {
                { mailQueue },
                { dbExecuteQueue },
                { requestQueue }
            };

            queueList.AsParallel().ForAll(i => i.Start()); // Bütün Queue'ları başlat
        }

        #region MailQueue Methods

        public static void AddMailQueue(MailDvo mailDvo)
        {
            mailQueue.Add(mailDvo);
        }

        public static void AddRangeMailQueue(ICollection<MailDvo> mailDvoCollection)
        {
            mailQueue.AddRange(mailDvoCollection);
        }

        #endregion

        #region DbExecuteQueue

        public void AddDbExecuteQueue(DbExecuteRequest req)
        {
            dbExecuteQueue.Add(req);
        }

        public void AddRange(ICollection<DbExecuteRequest> reqList)
        {
            dbExecuteQueue.AddRange(reqList);
        }

        #endregion

        #region HttpRequest

        public void AddHttpRequestQueue(RequestDvo request)
        {
            requestQueue.Add(request);
        }

        #endregion
    }
}
