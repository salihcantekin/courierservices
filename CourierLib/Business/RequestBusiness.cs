﻿using ServiceDvo;
using ServiceDvo.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CourierLib.Business
{
    public class RequestBusiness
    {
        public static RequestResult ExecuteRequest(RequestDvo value)
        {
            RequestResult Result = new RequestResult();


            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(value.Url);
            try
            {
                httpWebRequest.Method = value.Method;
                httpWebRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
                httpWebRequest.ContentType = value.ContentType;

                value.HeaderList?.ForEach(i => httpWebRequest.Headers.Add(i.Name, i.Value));

                String requestString = "";

                if (value.Content != null)
                {
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    using (StreamWriter writer = new StreamWriter(requestStream))
                    {
                        requestString = value.Content;
                        writer.Write(requestString);
                    }
                }

                HttpWebResponse webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
                Result.StatusCode = webResponse.StatusCode;

                using (Stream respStream = webResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(respStream, Encoding.UTF8))
                    {
                        Result.ResponseString = reader.ReadToEnd();
                    }
                }

                Result.IsSuccess = true;
            }
            catch (WebException webEx)
            {
                Result.SetError(webEx);

                using (var stream = webEx.Response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        Result.ResponseString = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Result.SetError(ex);
            }
            finally
            {
                httpWebRequest = null;
            }

            return Result;


            //String result = String.Empty;
            //try
            //{
            //    byte[] content = Encoding.UTF8.GetBytes(value.Content);

            //    HttpWebRequest request = WebRequest.Create(value.Url) as HttpWebRequest;
            //    request.Method = value.Method;
            //    request.ContentType = value.ContentType;
            //    request.ContentLength = content.Length;

            //    if (value.AcceptType != null)
            //        request.Accept = value.AcceptType;

            //    value.HeaderList?.ForEach(i => request.Headers.Add(i.Name, i.Value));

            //    request.Timeout = (int)TimeSpan.FromSeconds(Math.Max(value.TimeOut.Value, 300)).TotalMilliseconds;

            //    using (Stream Stream = request.GetRequestStream())
            //    {
            //        Stream.Write(content, 0, content.Length);
            //    }

            //    using (WebResponse response = request.GetResponse())
            //    {
            //        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            //        {
            //            result = sr.ReadToEnd();
            //        }
            //    };
            //}
            //catch (WebException webEx)
            //{
            //    using (var stream = webEx.Response.GetResponseStream())
            //    {
            //        using (var reader = new StreamReader(stream))
            //        {
            //            result = reader.ReadToEnd();
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //return result;
        }
    }
}
