﻿using ServiceDvo;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace CourierLib.Business
{
    public class MailBusiness
    {

        private void sendEMMail(MailDvo mailDvo)
        {
            //StringBuilder Result = new StringBuilder();
            //Auth authService = new Auth();
            //Post postService = null;

            //try
            //{
            //    EmAuthResult authResult = authService.Login(mailDvo.Profile.MailAccount.UserAuth.UserName, mailDvo.Profile.MailAccount.UserAuth.Password);
            //    if (!authResult.Code.Equals("00"))
            //    {
            //        Result.AppendFormat("#{0} EuroMessage Kullanıcı Doğrulaması Yapılamadı.\nHata: {1}", base.Guid.ToString(), authResult.Message);
            //    }

            //    String serviceTicket = authResult.ServiceTicket;

            //    while (ToAddresses.Count > 0)
            //    {
            //        String To = ToAddresses.FirstOrDefault();
            //        ToAddresses.Remove(To);

            //        if (!Utils.IsValidatedMail(To))
            //        {
            //            Result.AppendLine(String.Format("#{0} numaralı iş için alıcı mail adresi alanı boş yada yanlış.!", Guid.ToString()));
            //            continue;
            //        }

            //        EmPostResult postResult = null;
            //        try
            //        {
            //            #region Attachment Process

            //            List<EmAttachment> attList = new List<EmAttachment>
            //            (
            //                Attachments.
            //                    Values.
            //                    Select(i => i.GetLocalFilePath()).
            //                    Select(i => new EmAttachment()
            //                    {
            //                        Name = Path.GetFileNameWithoutExtension(i),
            //                        Type = Path.GetExtension(i).Replace(".", ""),
            //                        Content = File.ReadAllBytes(i)
            //                    })
            //            );

            //            #endregion

            //            postService = new Post();
            //            postResult = postService.PostHtml(serviceTicket,
            //                MailProfile.SenderDisplayName == "" ? MailProfile.MailAccount.SenderMail : MailProfile.SenderDisplayName,
            //                MailProfile.MailAccount.SenderMail,
            //                MailProfile.MailAccount.NoReplyMail,
            //                Subject,
            //                MailBody,
            //                MailProfile.MailAccount.Charset,
            //                To,
            //                To,
            //                attList.ToArray()
            //                );

            //            if (!postResult.Code.Equals("00"))
            //                Result.AppendFormat("#{0} Mail Gönderimi Sırasında Hata.({1})\nHata: {2}", base.Guid.ToString(), To, postResult.Message);

            //            // OnLog
            //        }
            //        catch (Exception Ex)
            //        {
            //            Result.Append(String.Format("#{0} {1} : Hata ({2})", base.Guid.ToString(), To, Ex.Message));
            //        }
            //        finally
            //        {
            //            postResult = null;
            //        }
            //    }
            //}
            //catch (Exception Ex)
            //{
            //    Result.Append(String.Format("#{0} {1} : Hata ({2})", base.Guid.ToString(), ToAddresses, Ex.Message));
            //}
            //finally
            //{
            //    authService = null;
            //    postService = null;
            //}
        }



        public static void SendMail(MailDvo mailDvo)
        {
            using (SmtpClient client = new SmtpClient())
            {
                client.Host = mailDvo.Account.HostName;
                client.Port = mailDvo.Account.Port;
                client.EnableSsl = mailDvo.Account.UseSsl;

                if (!String.IsNullOrEmpty(mailDvo.Account.UserAuth.UserName) && !String.IsNullOrEmpty(mailDvo.Account.UserAuth.Password))
                    client.Credentials = new NetworkCredential(mailDvo.Account.UserAuth.UserName, mailDvo.Account.UserAuth.Password);

                MailMessage message = new MailMessage()
                {
                    Body = mailDvo.MailBody,
                    From = new MailAddress(mailDvo.Account.SenderMail, mailDvo.SenderDisplayName ?? mailDvo.Account.SenderDisplayName),
                    Sender = new MailAddress(mailDvo.Account.SenderMail, mailDvo.SenderDisplayName ?? mailDvo.Account.SenderDisplayName),
                    Subject = mailDvo.Subject
                };

                mailDvo.To.ForEach(i => message.To.Add(i));
                mailDvo.CC?.ForEach(i => message.CC.Add(i));
                mailDvo.Bcc?.ForEach(i => message.Bcc.Add(i));

                message.Priority = (System.Net.Mail.MailPriority)mailDvo.Priority;
                message.ReplyToList.Add(mailDvo.Account.NoReplyMail);

                message.BodyEncoding =
                 message.HeadersEncoding =
                 message.SubjectEncoding = mailDvo.Encoding;

                client.Send(message);
            }
        }

        public static void AddQueue(MailDvo mailDvo)
        {
            QueueManager.AddMailQueue(mailDvo);
        }
    }
}
