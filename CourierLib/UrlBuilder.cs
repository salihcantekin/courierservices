﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib
{
    public class UrlBuilder: IDisposable
    {
        #region Definitions

        private Dictionary<String, String> List;
        private String Url;

        #endregion

        #region Constructor

        public UrlBuilder(String RootUrl)
        {
            List = new Dictionary<string, string>();
            Url = RootUrl;

            if (!String.IsNullOrEmpty(RootUrl) && !RootUrl.Contains('?'))
                Url = RootUrl + "?";
        }

        #endregion

        #region Public Methods

        public static UrlBuilder CreateNew(String RootUrl = "")
        {
            return new UrlBuilder(RootUrl);
        }

        public void Add(String ParameterName, String Value)
        {
            if (!List.ContainsKey(ParameterName))
                AddToList(ParameterName, Value);
        }

        public void Add(String ParameterName, int Value)
        {
            Add(ParameterName, Value.ToString());
        }

        public void Add(String ParameterName, String Value, bool OverrideIfExists)
        {
            if (OverrideIfExists && List.ContainsKey(ParameterName))
                List[ParameterName] = Value;
            else
                AddToList(ParameterName, Value);
        }

        public String Build(String Prefix = "")
        {
            if (List.Count == 0)
                return "";

            StringBuilder Result = new StringBuilder();
            Result.Append(Url);

            if (!String.IsNullOrEmpty(Prefix))
                Result.Append(Prefix);

            int Counter = 0;
            foreach (String Key in List.Keys)
            {
                if (Counter > 0)
                    Result.Append("&");

                Result.AppendFormat("{0}={1}", Key, List[Key]);
                Counter++;
            }

            return Result.ToString();
        }

        public void Clear()
        {
            List.Clear();
        }

        public String Get(String ParameterName)
        {
            if (List.ContainsKey(ParameterName))
                return List[ParameterName];
            else
                return String.Empty;
        }

        public String this[String ParameterName]
        {
            get { return Get(ParameterName); }
        }

        public bool Replace(String ParameterName, String Value)
        {
            if (List.ContainsKey(ParameterName))
                List[ParameterName] = Value;
            else
                return false;

            return true;
        }

        public bool Remove(String ParameterName)
        {
            if (List.ContainsKey(ParameterName))
                List.Remove(ParameterName);
            else
                return false;

            return true;
        }

        #endregion

        #region Private Methods

        private void AddToList(String ParameterName, String Value)
        {
            List.Add(ParameterName, Value);
        }

        public void Dispose()
        {
            Url = String.Empty;
            List = null;
        }

        #endregion
    }
}
