﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Interfaces
{
    public interface IQueue<T>: IBaseQueue
    {
        object WorkingOn(T value);

        bool Validate(T value);

        QueueState State { get; set; }

        void Add(T value);

        void AddRange(ICollection<T> value);

        void Remove(T value);
    }
}
