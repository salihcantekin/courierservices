﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Interfaces
{
    public interface IBaseQueue
    {
        void Start();

        void Stop();

        void ReStart();

        void Pause();

        void Resume();

        void BeginUpdate();

        void EndUpdate();

        bool Async { get; }
    }
}
