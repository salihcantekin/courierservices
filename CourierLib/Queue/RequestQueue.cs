﻿using CourierLib.Base;
using CourierLib.Business;
using CourierLib.Exceptions;
using ServiceDvo;
using ServiceDvo.Request;
using System;
using System.IO;
using System.Net;

namespace CourierLib.Queue
{
    public class RequestQueue : BaseQueue<RequestDvo>
    {
        public RequestQueue()
        {
            Async = true;
        }

        public override bool Validate(RequestDvo value)
        {
            if (value.Content == null)
                throw new ValidationException("Content cannot be null");

            if (value.Content.Length <= 0)
                throw new ValidationException("Content cannot be empty");

            return true;
        }

        public override object WorkingOn(RequestDvo value)
        {
            return RequestBusiness.ExecuteRequest(value);
        }
    }
}
