﻿using CourierLib.Base;
using CourierLib.Exceptions;
using ServiceDvo.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Queue
{
    public class DbExecuteSqlQueue : BaseQueue<DbExecuteRequest>
    {
        public override bool Validate(DbExecuteRequest value)
        {
            if (String.IsNullOrEmpty(value.ConnStr))
                throw new ValidationException("ConnectionString cannot be empty");

            if (String.IsNullOrEmpty(value.Sql))
                throw new ValidationException("Sql cannot be empty");

            return true;
        }

        public override object WorkingOn(DbExecuteRequest value)
        {
            //using (OraDb db = new OraDb(value.ConnStr))
            //{
            //    db.ExecuteNonQuery(value.Sql);
            //}

            return null;
        }
    }
}
