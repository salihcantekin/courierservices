﻿using CourierLib.Base;
using CourierLib.Business;
using CourierLib.Exceptions;
using ServiceDvo;
using System;
using System.Net;
using System.Net.Mail;

namespace CourierLib.Queue
{
    public class MailQueue : BaseQueue<MailDvo>
    {
        public override bool Validate(MailDvo value)
        {
            if (value == null)
                throw new ValidationException("Value cannot be null");

            if (value.Account == null)
                throw new ValidationException("Account cannot be null");

            if (String.IsNullOrEmpty(value.Account.HostName))
                throw new ValidationException("Host Name cannot be empty");

            if (value.To == null || value.To.Count <= 0)
                throw new ValidationException("Recipients must be filled");

            return true;
        }

        public MailQueue()
        {
            Async = true;
        }

        public override object WorkingOn(MailDvo value)
        {
            MailBusiness.SendMail(value);
            return null;
        }
    }
}