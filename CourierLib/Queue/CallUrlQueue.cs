﻿using CourierLib.Base;
using CourierLib.Exceptions;
using ServiceDvo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Queue
{
    public class CallUrlQueue : BaseQueue<CallUrlDvo>
    {
        public override bool Validate(CallUrlDvo value)
        {
            if (String.IsNullOrEmpty(value.Url))
                throw new ValidationException("Url must be filled");

            return true;
        }

        public override object WorkingOn(CallUrlDvo value)
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadString(value.Url);
            }
        }
    }
}