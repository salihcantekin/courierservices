﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Exceptions
{
    public class ValidationException: Exception
    {
        public ValidationException(String message): base(message)
        {

        }
    }
}
