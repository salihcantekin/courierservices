﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Events
{
    public class OnWorkingCompleteEventArgs<T> : EventArgs
    {
        public T Value { get; internal set; }

        public object Result { get; internal set; }
        public bool IsSuccess { get; internal set; }

        public String Message { get; internal set; }

        public String MessageDetail { get; internal set; }

        public OnWorkingCompleteEventArgs(T value):this()
        {
            Value = value;
        }

        public OnWorkingCompleteEventArgs()
        {

        }
    }
}
