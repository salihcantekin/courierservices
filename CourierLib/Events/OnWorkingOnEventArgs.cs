﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Events
{
    public class OnWorkingOnEventArgs<T> : EventArgs
    {
        public T Value { get; private set; }
        public object Result { get; private set; }

        public OnWorkingOnEventArgs(T value)
        {
            Value = value;
        }

        public OnWorkingOnEventArgs(T value, object result) : this(value)
        {
            Result = result;
        }
    }
}
