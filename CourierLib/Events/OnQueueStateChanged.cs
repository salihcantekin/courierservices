﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierLib.Events
{
    public class OnQueueStateChanged: EventArgs
    {
        public QueueState State { get; set; }

        public OnQueueStateChanged(QueueState state)
        {
            State = state;
        }
    }
}
