﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DatabaseLayer
{
	public class Utilities
	{
		public static object EncodingToIso8859(object value)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				StreamWriter writer = new StreamWriter(stream, Encoding.Default);
				XmlSerializer serializer = new XmlSerializer(value.GetType());
				serializer.Serialize(writer, value);
				stream.Flush();
				stream.Position = 0;
				var fileStream = new StreamReader(stream, Encoding.GetEncoding("ISO-8859-1"));
				object returnValue = serializer.Deserialize(fileStream);

				return returnValue;
			}
		}
	}
}
