﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DatabaseLayer
{
    public class DbTools
    {
        public static bool HasField(System.Data.Common.DbDataReader dr, String FieldName)
        {
            int fieldCount = dr.FieldCount;
            for (int i = 0; i < fieldCount; i++)
                if (dr.GetName(i).Equals(FieldName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            return false;
        }

        public static T Read<T>(DbDataReader dr, String fieldName, bool UseUtf8Encoding)
        {
            T Temp = default(T);

            if (dr.IsClosed || !dr.HasRows)
                return Temp;

            if (dr[fieldName] is DBNull)
                return Temp;

            if (typeof(T) == typeof(string) || typeof(T) == typeof(String))
            {
                Temp = (T)dr[fieldName];
            }
            else
            if (typeof(T) == typeof(int) || typeof(T) == typeof(long))
            {
                //Decimal Result = default(Decimal);
                //Result = (decimal)dr[fieldName];
                Temp = (T)Convert.ChangeType(dr[fieldName], typeof(T));
            }
            else
            if (typeof(T) == typeof(List<String>))
            {
                String Value = dr[fieldName].ToString();
                String[] arrayTemp = Value.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                List<String> Result = new List<string>();
                foreach (var item in arrayTemp)
                    Result.Add(item.Trim());

                Temp = (T)Convert.ChangeType(Result, typeof(T));
            }
            else
            if (typeof(T) == typeof(bool))
            {
                String Value = dr[fieldName].ToString().ToUpper();
                bool Result = false;
                Result = (Value.Equals("A") || Value.Equals("E") || Value.Equals("Y") || Value.Equals("1") || Value.Equals("TRUE"));
                Temp = (T)Convert.ChangeType(Result, typeof(T));
            }
            else
            if (typeof(T) == typeof(DateTime))
            {
                DateTime temp = DateTime.MinValue;
                String Value = dr[fieldName].ToString();
                if (DateTime.TryParse(Value, out temp))
                    Temp = (T)Convert.ChangeType(temp, typeof(T));
                else
                    Temp = (T)Convert.ChangeType(DateTime.MinValue, typeof(T));
            }
            else
            if (typeof(T) == typeof(Guid))
                Temp = (T)Convert.ChangeType(new Guid(dr[fieldName].ToString()), typeof(T));
            else
                Temp = (T)Convert.ChangeType(dr[fieldName], typeof(T));

            if (UseUtf8Encoding)
                Temp = (T)Utilities.EncodingToIso8859(Temp);
            return Temp;
        }

        public static T Read<T>(DbDataReader dr, String fieldName)
        {
            return Read<T>(dr, fieldName, false);
        }

        public static String Read(DbDataReader dr, String FieldName)
        {
            return Read<String>(dr, FieldName);
        }
    }
}
