﻿using System;
using System.Collections.Generic;
using System.Text;
//using System.Data.OracleClient;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;

namespace DatabaseLayer
{
    public static class DbLayer
    {
        private static Dictionary<DbDataReader, ConnectionPool> FConnections;
        public static ConnectionPool GetConnection(String ConnStr = null)
        {
            ConnectionPool Conn = new ConnectionPool(ConnStr);
            return Conn;
        }

        public static T Read<T>(DbDataReader dr, String fieldName)
        {
            T Temp = default(T);

            if (dr.IsClosed || !dr.HasRows)
                return Temp;

            if (dr[fieldName] is DBNull)
                return Temp;

            if (typeof(T) == typeof(string) || typeof(T) == typeof(String))
            {
                Temp = (T)dr[fieldName];
            }
            else
                if (typeof(T) == typeof(int) || typeof(T) == typeof(long))
            {
                Decimal Result = default(Decimal);
                Result = (decimal)dr[fieldName];
                Temp = (T)Convert.ChangeType(Result, typeof(T));
            }
            else
                    if (typeof(T) == typeof(List<String>))
            {
                String Value = dr[fieldName].ToString();
                String[] arrayTemp = Value.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                List<String> Result = new List<string>();
                foreach (var item in arrayTemp)
                    Result.Add(item);

                Temp = (T)Convert.ChangeType(Result, typeof(T));
            }
            else
                        if (typeof(T) == typeof(bool))
            {
                String Value = dr[fieldName].ToString().ToUpper();
                bool Result = false;
                Result = (Value.Equals("A") || Value.Equals("E") || Value.Equals("Y") || Value.Equals("1") || Value.Equals("TRUE"));
                Temp = (T)Convert.ChangeType(Result, typeof(T));
            }
            else
                            if (typeof(T) == typeof(DateTime))
            {
                DateTime temp = DateTime.MinValue;
                String Value = dr[fieldName].ToString();
                if (DateTime.TryParse(Value, out temp))
                    Temp = (T)Convert.ChangeType(temp, typeof(T));
                else
                    Temp = (T)Convert.ChangeType(DateTime.MinValue, typeof(T));
            }

            return Temp;
        }

        public static DbDataReader GetDataReader(StringBuilder Sql, String ConnStr = null)
        {
            return DbLayer.GetDataReader(Sql.ToString(), ConnStr);
        }

        public static DbDataReader GetDataReader(String Sql, String ConnStr = null)
        {
            ConnectionPool Pool = GetConnection(ConnStr);
            DbDataReader dr = Pool.GetDataReader(Sql);
            if (FConnections == null)
                FConnections = new Dictionary<DbDataReader, ConnectionPool>();
            FConnections.Add(dr, Pool);
            return dr;
        }

        public static int ExecSql(String Sql, String ConnStr = null)
        {
            int Value = 0;
            using (ConnectionPool Conn = GetConnection(ConnStr))
            {
                Value = Conn.ExecSql(Sql);
            }
            return Value;
        }

        public static DataTable GetDataTable(String Sql, String ConnStr = null)
        {
            DataTable dt = null;
            using (ConnectionPool Conn = GetConnection(ConnStr))
            {
                using (DbDataAdapter da = Conn.GetDataAdapter(Sql))
                {
                    dt = new DataTable();
                    da.Fill(dt);
                }
            }
            return dt;
        }

        public static int GetSequenceValue(String SequenceName, String ConnStr = null)
        {
            int Value = 0;
            using (ConnectionPool Conn = GetConnection(ConnStr))
            {
                Value = Conn.GetSequenceValue(SequenceName);
            }
            return Value;
        }

        public static void CloseReader(DbDataReader dr)
        {
            ConnectionPool Pool = null;
            bool Success = FConnections.TryGetValue(dr, out Pool);
            if (Success)
            {
                dr.Close();
                Pool.Dispose();
            }
        }

        public static bool HasField(System.Data.Common.DbDataReader dr, String FieldName)
        {
            int fieldCount = dr.FieldCount;
            for (int i = 0; i < fieldCount; i++)
                if (dr.GetName(i).Equals(FieldName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            return false;
        }
    }

    public class ConnectionPool : IDisposable
    {
        private List<DbCommand> cmmndList;
        public DbDataReader dr;

        OleDbConnection Conn;
        public ConnectionPool(String ConnStr = null)
        {
            cmmndList = new List<DbCommand>();
            Conn = new OleDbConnection();
            String connStr = ConnStr ?? "Provider=OraOLEDB.Oracle;Data Source=ETSX_8_IP;User Id=ETS_VNL;Password=ETS_VNL;";
            Conn.ConnectionString = connStr;
            Conn.Open();
        }

        public OleDbCommand CreateCommand()
        {
            OleDbCommand Cmmnd = new OleDbCommand();
            Cmmnd.Connection = Conn;
            cmmndList.Add(Cmmnd);
            return Cmmnd;
        }

        public OleDbCommand CreateCommand(String Sql)
        {
            OleDbCommand Cmmnd = this.CreateCommand();
            Cmmnd.Connection = Conn;
            Cmmnd.CommandText = Sql;
            return Cmmnd;
        }

        public OleDbCommand CreateCommand(StringBuilder Sql)
        {
            return this.CreateCommand(Sql.ToString());
        }

        public DbDataReader GetDataReader(String Sql)
        {
            OleDbCommand Cmmnd = Conn.CreateCommand();
            Cmmnd.CommandText = Sql;
            dr = Cmmnd.ExecuteReader();
            return dr;
        }

        public DbDataReader GetDataReader(StringBuilder Sql)
        {
            return GetDataReader(Sql.ToString());
        }

        public int ExecSql(String Sql)
        {
            OleDbCommand Cmmnd = CreateCommand(Sql);
            int Value = Cmmnd.ExecuteNonQuery();
            return Value;
        }

        public int GetSequenceValue(String SequenceName)
        {
            String Sql = String.Format("SELECT {0}.NEXTVAL AS SEQ FROM DUAL", SequenceName);
            OleDbCommand Cmmnd = CreateCommand(Sql);
            object obj = Cmmnd.ExecuteScalar();
            int Value = Convert.ToInt32(obj);
            return Value;
        }

        public OleDbDataAdapter GetDataAdapter(String Sql)
        {
            OleDbCommand Cmmnd = CreateCommand(Sql);
            OleDbDataAdapter da = new OleDbDataAdapter(Cmmnd);
            return da;
        }

        public bool CloseAll()
        {
            try
            {
                while (cmmndList.Count > 0)
                {
                    cmmndList[0].Dispose();
                    cmmndList.RemoveAt(0);
                }

                if (Conn.State == System.Data.ConnectionState.Open)
                    Conn.Close();

                Conn.Dispose();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Dispose()
        {
            CloseAll();
        }
    }
}
