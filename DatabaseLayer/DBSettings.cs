﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseLayer
{
	public class DBSettings
	{
		public event EventHandler<Events.OnDatabaseError> OnError;

		public DBSettings()
		{
			ConnStrList = new Dictionary<string, string>();
		}

		private static DBSettings setting;
		public static DBSettings Settings
		{
			get
			{
				if (setting == null)
					setting = new DBSettings();

				return setting;
			}
		}

		private Dictionary<String, String> ConnStrList;
		public String DefaultConnStr { get; set; }

		public String GetConnStr(String Key)
		{
			String Result = String.Empty;
			ConnStrList.TryGetValue(Key, out Result);
			return Result ?? "";
		}

		public String GetKey(String ConnStr)
		{
			String Result = String.Empty;
			Result = ConnStrList.Keys.FirstOrDefault(i => i.Equals(ConnStr));
			return Result ?? "";
		}

		public bool AddConnStr(String Name, String ConnStr)
		{
			try
			{
				ConnStrList.Add(Name, ConnStr);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public bool RemoveConnStr(String Key)
		{
			try
			{
				ConnStrList.Remove(Key);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public bool ClearList()
		{
			try
			{
				if (ConnStrList.Count > 0)
					ConnStrList.Clear();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public void TriggerError(Exception Ex)
		{
            OnError?.Invoke(this, new Events.OnDatabaseError(Ex));
        }
	}
}
