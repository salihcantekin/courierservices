﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
	public abstract class DBProviderBase
	{
		public DBProviderBase(String ConnectionString)
		{
			try
			{
				Connection = CreateConnection();
				Connection.ConnectionString = ConnectionString;
				Connection.Open();
			}
			catch (Exception Ex)
			{
				DBSettings.Settings.TriggerError(Ex);
				throw Ex;
			}
		}

		public abstract DbDataReader GetData(string sql, DBParameterCollection parameters = null);
		public abstract object ExecuteScalar(string sql, DBParameterCollection parameters = null);
		protected internal abstract DbConnection CreateConnection();
		protected internal DbConnection Connection { get; set; }
		public virtual object GetValueFromFunction(string sql, DBParameterCollection parameters = null) { return null; }
		public abstract int ExecuteNonQuery(string sql, DBParameterCollection parameters = null);
		public abstract int ExecuteNonQueryForProcedure(string sql, DBParameterCollection parameters = null);
		public abstract string Prefix { get; }

		protected internal void PrepareQueryParameters(ref string sql, DBParameterCollection parameters = null)
		{
			if (parameters == null)
				return;

			foreach (var prm in parameters)
			{
				sql = sql.Replace("@" + prm.Name, Prefix + prm.Name);
			}
		}
	}
}
