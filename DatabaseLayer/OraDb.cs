﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace DatabaseLayer
{
    public class OraDb : DBProviderBase, IDisposable
    {
        public OraDb(String ConnectionString)
            : base(ConnectionString)
        {
            if (String.IsNullOrEmpty(ConnectionString))
                throw new Exception("Connection Parametresi Boş Olamaz");
        }

        public OraDb()
            : this(DBSettings.Settings.DefaultConnStr)
        {

        }

        public override System.Data.Common.DbDataReader GetData(string sql, DBParameterCollection parameters = null)
        {
            try
            {
                PrepareQueryParameters(ref sql, parameters);
                System.Data.Common.DbCommand cmmnd = new OleDbCommand(sql, (OleDbConnection)Connection);
                RetreiveParameters(cmmnd.Parameters, parameters);
                //cmmnd.BindByName = true;

                cmmnd.CommandType = System.Data.CommandType.Text;
                System.Data.Common.DbDataReader data = cmmnd.ExecuteReader();

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RetreiveParameters(DBParameterCollection parameters, OleDbParameterCollection oracleParameterCollection)
        {
            foreach (OleDbParameter parameter in oracleParameterCollection)
            {
                if (parameter.Direction == System.Data.ParameterDirection.Output)
                    parameters[parameter.ParameterName].Value = parameter.Value;
            }
        }

        private void RetreiveParameters(System.Data.Common.DbParameterCollection oracleParameterCollection, DBParameterCollection parameters)
        {
            if (parameters == null)
                return;

            foreach (DBParameter parameter in parameters)
            {
                OleDbParameter newParameter = new OleDbParameter();
                if (parameter.Size > 0)
                    newParameter.Size = parameter.Size;
                newParameter.ParameterName = parameter.Name;
                if (parameter.Value == null)
                    newParameter.Value = DBNull.Value;
                else
                {
                    newParameter.Value = parameter.Value;
                    //newParameter.OracleDbType = GetOracleDbType(parameter.Value);
                }

                //newParameter.OracleDbType = GetDbType(parameter.DbType);
                newParameter.Direction = parameter.Direction;

                oracleParameterCollection.Add(newParameter);
            }
        }

        public override object ExecuteScalar(string sql, DBParameterCollection parameters = null)
        {
            PrepareQueryParameters(ref sql, parameters);

            using (OleDbCommand command = new OleDbCommand(sql, Connection as OleDbConnection))
            {
                //command.BindByName = true;
                RetreiveParameters(command.Parameters, parameters);
                object data = command.ExecuteScalar();
                return data;
            }
        }

        protected internal override System.Data.Common.DbConnection CreateConnection()
        {
            return new OleDbConnection();
        }

        public override int ExecuteNonQuery(string sql, DBParameterCollection parameters = null)
        {
            try
            {
                PrepareQueryParameters(ref sql, parameters);
                using (OleDbCommand command = new OleDbCommand(sql, Connection as OleDbConnection))
                {
                    this.RetreiveParameters(command.Parameters, parameters);
                    //command.BindByName = true;
                    int result = command.ExecuteNonQuery();
                    RetreiveParameters(parameters, command.Parameters);
                    return result;
                }
            }
            catch (OleDbException ex)
            {
                throw ex;
            }
        }

        public override int ExecuteNonQueryForProcedure(string sql, DBParameterCollection parameters = null)
        {
            PrepareQueryParameters(ref sql, parameters);
            using (OleDbCommand command = new OleDbCommand(sql, Connection as OleDbConnection))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;
                RetreiveParameters(command.Parameters, parameters);
                //command.BindByName = true;
                int result = command.ExecuteNonQuery();
                RetreiveParameters(parameters, command.Parameters);
                return result;
            }
        }

        public override string Prefix
        {
            get { return ":"; }
        }

        public override object GetValueFromFunction(string sql, DBParameterCollection parameters = null)
        {
            DBParameter Param = new DBParameter();
            Param.Direction = System.Data.ParameterDirection.ReturnValue;
            Param.Size = 16;
            Param.Name = "return_value";

            parameters.Add(Param);

            PrepareQueryParameters(ref sql, parameters);

            using (OleDbCommand command = new OleDbCommand(sql, Connection as OleDbConnection))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;
                RetreiveParameters(command.Parameters, parameters);
                //command.BindByName = true;
                command.ExecuteNonQuery();
                return command.Parameters["return_value"].Value;
            }
        }

        public int GetSeqValue(String SequenceName)
        {
            object obj = this.ExecuteScalar(String.Format("SELECT {0}.NEXTVAL FROM DUAL", SequenceName), null);
            return int.Parse(obj.ToString());
        }

        public void Dispose()
        {
            base.Connection.Close();
            base.Connection = null;
        }
    }
}
