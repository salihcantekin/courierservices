﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer.Events
{
	public class OnDatabaseError: EventArgs
	{
		public String ExMessage;
		public Exception Exception;

		public OnDatabaseError(Exception Ex)
		{
			this.Exception = Ex;
			this.ExMessage = Ex.Message;
		}
	}
}
