﻿using DatabaseLayer.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DatabaseLayer.SqlGenerator
{
    public class DeleteSqlGenerator : BaseSqlGenerator
    {
        public DeleteSqlGenerator(string TableName) : base(TableName)
        {
        }

        public DeleteSqlGenerator()
        {

        }

        public override string Generate()
        {
            base.Generate();
            return $"DELETE {TableName} WHERE {GenerateConditions()}";
        }
    }
}