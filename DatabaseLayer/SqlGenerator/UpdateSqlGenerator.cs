﻿using DatabaseLayer.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DatabaseLayer.SqlGenerator
{
    public class UpdateSqlGenerator : BaseSqlGenerator
    {
        public UpdateSqlGenerator(String TableName) : base(TableName)
        {
        }

        

        public override String Generate()
        {
            base.Generate();

            return $"UPDATE {TableName} SET {GenerateParams()} WHERE {GenerateConditions()}";
        }

    }
}