﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
	public class DBParameterCollection : KeyedCollection<string, DBParameter>, IDisposable
	{
		protected override string GetKeyForItem(DBParameter item)
		{
			return item.Name;
		}

		public void Add(string parameterName, object value)
		{
			DBParameter parameter = new DBParameter();
			parameter.Name = parameterName;
			parameter.Value = value;
			this.Add(parameter);
		}

		public void Add(string parameterName, object value, DBType dbType)
		{
			DBParameter parameter = new DBParameter();
			parameter.Name = parameterName;
			parameter.Value = value;
			parameter.DbType = dbType;
			this.Add(parameter);
		}

		public List<DBParameter> GetOutputParameters()
		{
			return this.Where(x => x.Direction != System.Data.ParameterDirection.Input).ToList();
		}

        public void Dispose()
        {
            Clear();
        }
    }
}
