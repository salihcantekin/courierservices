﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
	public class DBParameter
	{
		public DBParameter()
		{
			Direction = System.Data.ParameterDirection.Input;
		}

		public string Name { get; set; }
		public DBType DbType { get; set; }
		public System.Data.ParameterDirection Direction { get; set; }
		public object Value { get; set; }
		public int Size { get; set; }

		//public EntityPropertyMap MapProperty { get; set; }
	}
}
