﻿
using DBManager;
using ServiceTest.DefinitionService;
using ServiceTest.MailService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceTest
{
    class Program
    {

        static DefinitionServiceClient defService = new DefinitionServiceClient();


        static void Main(string[] args)
        {


            //defService.Url = "http://9.1.1.62:1002/DefinitionsService.svc";
            //service.Url = "http://localhost:1002/DefinitionsService.svc";

            System.Net.ServicePointManager.Expect100Continue = false;

            MailServiceClient mailService = new MailServiceClient();

            try
            {
                ResultOfArrayOfMailAccountWN3Xzys3 mailAccountRes = mailService.GetMailAccounts();

                if (mailAccountRes.IsSuccess)
                {
                    MailDvo mail = new MailDvo
                    {
                        IsHtmlBody = false,
                        MailBody = "Test İçerik",
                        Priority = MailPriority.High,
                        SenderDisplayName = "Salih Cantekin",
                        Subject = "Subject",
                        To = new[] { "salihcantekin@gmail.com" }
                    };

                    mail.Account = mailAccountRes.Value.FirstOrDefault(i => i.Name.Equals("ETS_CC"));
                    
                    ResultOfMailResultwiXOxd2K mailRes = mailService.Send(mail);
                }

                Console.WriteLine("\n\n\n\n\nWaiting for any key");
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception");
                Console.WriteLine(Ex.ToString());
            }
            
            Console.ReadKey();
        }
        
    }
}
